Except for the directory `python`, the original files (i.e. the obtained and converted datafiles) are not save with git due to their file size. However a list of the directory content is stored in each of the subdirectories and is saved with git.

Note in the subdirectories with datafiles, the subdirs monthly and Yearly contain those obtained from Tim Hessels and are all tifs in EPSG:32636. The files in monthly_model_epsg6984 are all tifs generated using the code in the subdirectory python and match exactly the center of the cells of the model. The latter tifs are in datum EPSG:6984.

TO 2021-11-26