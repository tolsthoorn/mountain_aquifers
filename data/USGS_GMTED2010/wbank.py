"""

# Working with the wbank dem

# Area of interest (Mountain aqufier Israel / Palestine, West Bank)
31.3N - 32.6N # Decimal degrees
34.6E - 35.6E # Decimal degrees

The rasterio is run in its own environment, which was required to prevent dependeccy
issues with the base environment of conda.

@author: Theo
"""
#%%
import os
import sys
import rasterio
from rasterio.plot import show
import numpy as np
import matplotlib.pyplot as plt
import mkpath # module to extent sys.path with path to extra self-made modules
import etc # one of the self-made modules
from etc import newfig
import logging

logging.disable(sys.maxsize)

#%% set the directories and navigate to the workspace with this python file

proj_dir = "/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/"
dems_dir = os.path.join(proj_dir, 'data/USGS_GMTED2010/gmted2010/')
dem300_dir = os.path.join(dems_dir, 'GMTED2010N30E030_300/')
ws = os.path.join(proj_dir, 'data/USGS_GMTED2010/')
os.chdir(ws)

#%% Reading the wbank data

# Region of interest
region = {'west': 34.5, 'east': 35.6, 'south': 31.3, 'north': 32.6}

wbank = rasterio.open('wbank.tif')
rasterio.plot.show(wbank)

elev = wbank.read(1)
# %% So far so good. Now convert to israeli coordinates

from rasterio.warp import calculate_default_transform, reproject, Resampling

src = wbank
dst_crs = 'EPSG:6984'

with rasterio.open('wbank.tif') as src:
    transform, width, height = calculate_default_transform(
        src.crs, dst_crs, src.width, src.height, *src.bounds)
    kwargs = wbank.meta.copy()
    kwargs.update({
        'crs': dst_crs,
        'transform': transform,
        'width': width,
        'height': height
    })

    with rasterio.open('wbank_6984.tif', 'w', **kwargs) as dst:
        for i in range(1, wbank.count + 1):
            reproject(
                source=rasterio.band(src, i),
                destination=rasterio.band(dst, i),
                src_transform=src.transform,
                src_crs=src.crs,
                dst_transform=transform,
                dst_crs=dst_crs,
                resampling=Resampling.nearest)# %%

# %%
wbank_6984 = rasterio.open('wbank_6984.tif')
rasterio.plot.show(wbank_6984)

elev = wbank_6984.read(1)

# %%
wbank_6984.meta
# %% Now get the elevation of the cells of the model of Yasin

# We can interpolate to the coordinates of the resampled cells.
# First get the grid  coordinates in a grid object from the original model

from read_pmw_model import yasin_mf6, yasin_old, gr as gr_model


# %%
gr_model.xm

a = wbank_6984.transform
# %%
# %%
