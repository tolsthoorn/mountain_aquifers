#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" 
# set_paths.py

# Small module to add directories to sys.path specific to this workspace
# This was a brilliant idea found in
# https://stackoverflow.com/questions/15208615/using-pth-files
"""

import sys
import os

#sys.path.append('/whatever/dir/you/want')
hygea_dir   = '/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/'
proj_dir    = os.path.join(hygea_dir, '2020_BUZA_ISR_PAL_T/')
models_dir  = os.path.join(proj_dir, 'models/')
model_dir   = os.path.join(models_dir, 'Yasin_1999_Eastern_Basin/')
src_dir     = os.path.join(model_dir, 'python/')
data_dir    = os.path.join(models_dir, 'data/')
dems_dir    = os.path.join(data_dir, 'USGS_GMTED2010/')
pmwin_dir   = os.path.join(model_dir, 'pm5_1_model_files/')
test_dir    = os.path.join(src_dir, 'tests/')
gis_dir     = os.path.join(models_dir, 'QGIS')

exe_dir     = '/Users/Theo/GRWMODELS/mfLab/trunk/bin/'
tools_dir   = '/Users/Theo/GRWMODELS/python/tools/'

if not tools_dir in sys.path:
    sys.path.append(tools_dir)

if not src_dir in sys.path:
    sys.path.append(src_dir)

gr_surface_tif = os.path.join(dems_dir, 'mdl_elev.tif')

for d in [hygea_dir, proj_dir, models_dir, model_dir, src_dir,   
                    data_dir, dems_dir, pmwin_dir, test_dir, gis_dir]:
    assert os.path.isdir(d) 

os.chdir(src_dir)