#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 29 00:55:23 2021

# The digital elevation file have been downloaded from my registered account at EROS (lastpass)

# Area of interest
31.3N - 32.6N # Decimal degrees
34.6E - 35.6E # Decimal degrees


@author: Theo
"""
#%%
import os
import sys
import rasterio
from rasterio.plot import show
import numpy as np
import matplotlib.pyplot as plt
import mkpath
import etc

#%%
#HOME = Get th

region = {'west': 34.5, 'east': 35.6, 'south': 31.3, 'north': 32.6}

#%% Get the dems from gmted2010 (USGS global dem with 30 arc resulution)

#
proj_dir = "/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/"
dems_dir = os.path.join(proj_dir, 'data/USGS_GMTED2010/gmted2010/')
dem300_dir = os.path.join(dems_dir, 'GMTED2010N30E030_300/')
ws = os.path.join(proj_dir, 'data/USGS_GMTED2010/')
os.chdir(ws)

tifs = [
    '30n030e_20101117_gmted_dsc300.tif', # conflated file, with line emphasis, best of rivers and ridges
    '30n030e_20101117_gmted_med300.tif', # median
    '30n030e_20101117_gmted_max300.tif', # maximum
    '30n030e_20101117_gmted_min300.tif', # minimum
    '30n030e_20101117_gmted_mea300.tif', # mean, best for 3D view most accurate
    '30n030e_20101117_gmted_std300.tif', # std deviation
    ]

# Notice that the outer pixcels are centered at the coordinates and the
# right most col and top row are omitted.
# This implies that the height and width are exactly equal to the diff of the tile coordinates

#%% Get the dsc file

fname =os.path.join(dem300_dir, tifs[0])
dataset = rasterio.open(fname)
show(dataset)


#%%

# This way one can get info from the dataset:
dataset.name
dataset.mode   # 'r'
dataset.count  # 1
dataset.width  # 3600
dataset.height # 2400

# Comprehension of
# Dataset indexes and dataset.types
#{i: dtype for i, dtype in zip(dataset.indexes, dataset.dtypes)}

dataset.bounds # BoundingBox(left=29.999861111111112, bottom=29.99986111111111, right=59.999861111111116, top=49.99986111111111)

#%% Reading raster data

dataset.indexes # (1,)

# Read the entire dataset (band1)
band1 = dataset.read(1)

# Just for fun, get the height of a point in the center
band1[dataset.height // 2, dataset.width // 2] # elevation of point in center

# This shows how row and col index can be obtained from coordinates
row, col = dataset.index(30.0, 31.0)

# Here we get the indices of the ul anf lr points of the dataset we're interested in
row1, col1 = dataset.index(region['west'], region['north'])  # Upper left
row2, col2 = dataset.index(region['east'], region['south'])  # Lower right

print("Data read:")
print(row1, col1)
print(row2, col2)


#%% Transform

# This is the general data transform from indices naar lat lon. The 0.008.. and -0.008 multiplies
# the x indices by pixelwidth and the y by -pixelheight, after which the coordinates of the ul are
# added. So the center of the upper left pixcel is 30E, 50N
dataset.transform # Affine(0.008333333333333333, 0.0, 29.999861111111112,
                  # 0.0, -0.008333333333333333, 49.99986111111111)
dataset.transform * (0,0) # upper left pixel coordinates
dataset.transform * (dataset.width, dataset.height)

dataset.crs # CRS.from_epsg(4326)

#%%
wbank = band1[row1:row2, col1:col2]

ax = etc.newfig("Part of the dem, now over Mountain aquifer", "x", "y")
ax.imshow(wbank)
#%%
transf = rasterio.Affine(0.008333333333333333, 0.0, region['west'],
                0.0, -0.008333333333333333, region['north'])

with rasterio.open() as src_dataset:
    with rasterio.open('wbank_dsc.tif', 'w') as des_dataset:
        kwds = dataset.profile
        kwds['transform'] = transf
        kwds['height'], kwds['width'] = wbank.shape
        kwds['count']=1
        #kwds['crs'] = CRS.from_epsg(4326)
# %%
