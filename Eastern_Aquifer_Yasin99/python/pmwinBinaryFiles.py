# %% imports
import numpy as np
import os
import struct
from warnings import warn

struct_formats = { # Note length -1 means "undefined or not fixed. See module Python module `struct`"
    "x": {"ctype": "pad byte", "python type": "no value", "standard size": -1, "Notes": ""},
    "c": {"ctype": "char", "python type": "bytes of length 1", "standard size": 1, "Notes": ""},
    "b": {"ctype": "signed char", "python type": "integer", "standard size": 1, "Notes": "(1) , (2)"},
    "B": {"ctype": "unsigned char", "python type": "integer", "standard size": 1, "Notes": "(2)"},
    "?": {"ctype": "_Bool", "python type": "bool", "standard size": 1, "Notes": "(1)"},
    "h": {"ctype": "short", "python type": "integer", "standard size": 2, "Notes": "(2)"},
    "H": {"ctype": "unsigned short", "python type": "integer", "standard size": 2, "Notes": "(2)"},
    "i": {"ctype": "int", "python type": "integer", "standard size": 4, "Notes": "(2)"},
    "I": {"ctype": "unsigned int", "python type": "integer", "standard size": 4, "Notes": "(2)"},
    "l": {"ctype": "long", "python type": "integer", "standard size": 4, "Notes": "(2)"},
    "L": {"ctype": "unsigned long", "python type": "integer", "standard size": 4, "Notes": "(2)"},
    "q": {"ctype": "long", "python type": "long integer", "standard size": 8, "Notes": "(2)"},
    "Q": {"ctype": "unsigned long", "python type": "long integer", "standard size": 8, "Notes": "(2)"},
    "n": {"ctype": "ssize_t", "python type": "integer", "standard size": -1, "Notes": "(3)"},
    "N": {"ctype": "size_t", "python type": "integer", "standard size": -1, "Notes": "(3)"},
    "e": {"ctype": "(6)", "python type": "float", "standard size": 2, "Notes": "(4)"},
    "f": {"ctype": "float", "python type": "float", "standard size": 4, "Notes": "(4)"},
    "d": {"ctype": "double", "python type": "float", "standard size": 8, "Notes": "(4)"},
    "s": {"ctype": "char[]", "python type": "bytes", "standard size": -1, "Notes": ""},
    "p": {"ctype": "char[]", "python type": "bytes", "standard size": -1, "Notes": ""},
    "P": {"ctype": "void *", "python type": "integer", "standard size": -1, "Notes": "(5)"},
}

# %% 
LEGAL_PMWIN_TYPES = {'CBC', 'ZONE', 'ASCII', 'BINARY'}

pkgs_const = { # keys are file extensions all with dot and upper()
".TOP": {"type": "CBC", "descr": "Top elevation of layers"},
".BOT": {"type": "CBC", "descr": "Bottom elevation of layers"},
".IBD": {"type": "CBC", "descr": "IBOUND array"},
".POR": {"type": "CBC", "descr": "Effective Porosity"},
".STO": {"type": "CBC", "descr": "Specific storage"},
".CON": {"type": "CBC", "descr": "Horizontal hydraulic conductivity"},
".LEA": {"type": "CBC", "descr": "Vertical hydraulic conductivity"},
".HEA": {"type": "CBC", "descr": "Starting head"},
".TIC": {"type": "CBC", "descr": "ICBUND Array (MT3D)"},
".TSC": {"type": "CBC", "descr": "Starting Concentration (MT3D)"},
".TOB": {"type": "CBC", "descr": "Observation Points (MT3D)"},
".LKN": {"type": "CBC", "descr": "Vertical leakance"},
".DWA": {"type": "CBC", "descr": "Wetting threshold (MODFLOW BCF2)"},
".TAL": {"type": "CBC", "descr": "Longitudinal dispersivity (MT3D)"},
".HTC": {"type": "CBC", "descr": "Transmissivity"},
".SCC": {"type": "CBC", "descr": "Confined storage coefficient"},
".YLD": {"type": "CBC", "descr": "Specific Yield"},
".WAL": {"type": "CBC", "descr": "Direction (horizontal flow barrier)"},
".WAC": {"type": "CBC", "descr": "Kf/Wall_thickness (horizontal flow barrier)"},
".WBL": {"type": "CBC", "descr": "Subregions for the water budget calculation"},
"._83": {"type": "CBC", "descr": "Recycle"},
".C85": {"type": "CBC", "descr": "Location of reservoirs (PMWIN 4.1 or higher)"},
".C86": {"type": "CBC", "descr": "Bottom Elevation of the reservoir (PMWIN 4.1 or higher)"},
".C87": {"type": "CBC", "descr": "Bed conductivity of the reservoir (PMWIN 4.1 or higher)"},
".C88": {"type": "CBC", "descr": "Bed Thickness of the reservoir (PMWIN 4.1 or higher)"},
".C89": {"type": "CBC", "descr": "Layer indicator of the reservoir (PMWIN 4.1 or higher)"},
".C91": {"type": "CBC", "descr": "Bulk density of the soil matrix (PMWIN 4.1 or higher)"},
".C92": {"type": "CBC", "descr": "First sorption constant (PMWIN 4.1 or higher)"},
".C93": {"type": "CBC", "descr": "Second sorption constant (PMWIN 4.1 or higher)"},
".C94": {"type": "CBC", "descr": "First-order rate constant for the dissolved phase (PMWIN 4.1 or higher)"},
".C95": {"type": "CBC", "descr": "First-order rate constant for the sorbed phase (PMWIN 4.1 or higher)"},
".C97": {"type": "CBC", "descr": "Digitizer (PMWIN 4.1 or higher)"},
}

pkgs = { # keys are file extensions all with dot and upper()
".XY":  {"type": "ASCII", "descr": "Cell sizes and numbers of cells and layers", "pkg": "Geometry and Boundary Conditions"},
".BOT": {"type": "CBC", "descr": "Elevation of the bottom of layers", "pkg": "Geometry and Boundary Conditions"},
".IBD": {"type": "CBC", "descr": "IBOUND matrix (used by MODFLOW and MOC3D)", "pkg": "Geometry and Boundary Conitions"},
".TIC": {"type": "CBC", "descr": "ICBUND matrix (used by MT3D and MT3DMS)", "pkg": "Geometry and Boundary Conditions"},
".TOP": {"type": "CBC", "descr": "Elevation of the top of layers", "pkg": "Geometry and Boundary Conditions"},
".HEA": {"type": "CBC", "descr": "Initial hydraulic heads", "pkg": "Initial Values"},
".TSC": {"type": "CBC", "descr": "Initial concentration used by MT3D and MOC3", "pkg": "Initial Values"},
".CON": {"type": "CBC", "descr": "Horizontal hydraulic conductivity", "pkg": "Aquifer Parameters"},
".HTC": {"type": "CBC", "descr": "Transmissivity", "pkg": "Aquifer Parameters"},
".LEA": {"type": "CBC", "descr": "Vertical hydraulic conductivity", "pkg": "Aquifer Parameters"},
".LKN": {"type": "CBC", "descr": "Vertical leakance", "pkg": "Aquifer Parameters"},
".POR": {"type": "CBC", "descr": "Effective porosity", "pkg": "Aquifer Parameters"},
".SCC": {"type": "CBC", "descr": "Storage coefficient", "pkg": "Aquifer Parameters"},
".STO": {"type": "CBC", "descr": "Specific storage", "pkg": "Aquifer Parameters"},
".TAL": {"type": "CBC", "descr": "Longitudinal dispersivity", "pkg": "Aquifer Parameters"},
".YLD": {"type": "CBC", "descr": "Specific yield", "pkg": "Aquifer Parameters"},
".63": {"type": "CBC", "descr": "Parameter numbers associated with horizontal hydraulic conductivity", "pkg": "Aquifer Parameters"},
".64": {"type": "CBC", "descr": "Parameter numbers associated with vertical hydraulic conductivity", "pkg": "Aquifer Parameters"},
".65": {"type": "CBC", "descr": "Parameter numbers associated with specific storage", "pkg": "Aquifer Parameters"},
".66": {"type": "CBC", "descr": "Parameter numbers associated with transmissvity", "pkg": "Aquifer Parameters"},
".67": {"type": "CBC", "descr": "Parameter numbers associated with vertical leakance", "pkg": "Aquifer Parameters"},
".68": {"type": "CBC", "descr": "Parameter numbers associated with storage coefficient", "pkg": "Aquifer Parameters"},
".69": {"type": "CBC", "descr": "Parameter numbers associated with specific yield", "pkg": "Aquifer Parameters"},
".DWA": {"type": "CBC", "descr": "Wetting threshold.", "pkg": "BCF2 Package"},
".C37": {"type": "CBC", "descr": "Cell density.", "pkg": "Density Package"},
".DRC": {"type": "CBC", "descr": "Hydraulic conductance of the interface between an aquifer and a drain", "pkg": "Drain Package"},
".DRE": {"type": "CBC", "descr": "Elevation of drain", "pkg": "Drain Package"},
".291": {"type": "CBC", "descr": "Parameter numbers associated with drains", "pkg": "Drain Package"},
".EET": {"type": "CBC", "descr": "Maximum evapotranspiration rate [L/T]", "pkg": "Evapotranspiration Package"},
".EIE": {"type": "CBC", "descr": "Layer indicator array. For each horizontal location, it indicates the layer from which evapotranspiration is removed", "pkg": "Evapotranspiration Package"},
".ESU": {"type": "CBC", "descr": "Elevation of the evapotranspiration surface", "pkg": "Evapotranspiration Package"},
".EXD": {"type": "CBC", "descr": "Evapotranspiration extinction depth", "pkg": "Evapotranspiration Package"},
".292": {"type": "CBC", "descr": "Parameter numbers associated with EVP cells", "pkg": "Evaporation Package"},
".GHB": {"type": "CBC", "descr": "Head on the general-head-boundary", "pkg": "General-Head Boundary Package"},
".GHC": {"type": "CBC", "descr": "Hydraulic conductance of the interface between the aquifer cell and the general-head boundary", "pkg": "General-Head Boundary Package"},
".293": {"type": "CBC", "descr": "Parameter numbers associated with GHB cells", "pkg": "General-Head Boundary Package"},
".WAL": {"type": "CBC", "descr": "Direction of a horizontal-flow barrier", "pkg": "Horizontal-Flow Barriers Package"},
".WAC": {"type": "CBC", "descr": "Hydraulic conductivity divided by the thickness of a horizontal-flow barrier", "pkg": "Horizontal-Flow Barriers Package"},
".IB1": {"type": "CBC", "descr": "Preconsolidation Head", "pkg": "Interbed Storage Package"},
".IB2": {"type": "CBC", "descr": "Elastic Storage Factor", "pkg": "Interbed Storage Package"},
".IB3": {"type": "CBC", "descr": "Inelastic Storage Factor", "pkg": "Interbed Storage Packae"},
".IB4": {"type": "CBC", "descr": "Starting Compaction", "pkg": "Interbed Storage Package"},
".294": {"type": "CBC", "descr": "Parameter numbers associated with IBS cells", "pkg": "Interbed Storage Package"},
".RCH": {"type": "CBC", "descr": "Recharge flux [L/T]", "pkg": "Recharge Package"},
".RCI": {"type": "CBC", "descr": "Layer indicator array that defines the layer in each vertical column where recharge is applied", "pkg": "Recharge Package"},
".295": {"type": "CBC", "descr": "Parameter numbers associated with RCH cells", "pkg": "Recharge Package"},
".C85": {"type": "CBC", "descr": "Location of reservoirs", "pkg": "Reservoir Package"},
".C86": {"type": "CBC", "descr": "Bottom elevation of reservoir", "pkg": "Reservoir Package"},
".C87": {"type": "CBC", "descr": "Bed conductivity of reservoir", "pkg": "Reservoir Package"},
".C88": {"type": "CBC", "descr": "Bed thickness of reservoir", "pkg": "Reservoir Package"},
".C89": {"type": "CBC", "descr": "Layer indicator of reservoir", "pkg": "Reservoir Package"},
".296": {"type": "CBC", "descr": "Parameter numbers associated with reservoir cells", "pkg": "Reservoir Package"},
".RIC": {"type": "CBC", "descr": "Hydraulic conductance of riverbed", "pkg": "River Package"},
".RIR": {"type": "CBC", "descr": "Elevation of the bottom of riverbed", "pkg": "River Packae"},
".RIS": {"type": "CBC", "descr": "Water surface elevation of river", "pkg": "River Package"},
".297": {"type": "CBC", "descr": "Parameter numbers associated with RIV cells", "pkg": "River Package"},
".SBO": {"type": "CBC", "descr": "Elevation of the bottom of the streambed", "pkg": "Streamflow-Routine Package"},
".SEG": {"type": "CBC", "descr": "Segment number - sequential number assigned to a group of reaches", "pkg": "Streamflow-Routine Package"},
".SFL": {"type": "CBC", "descr": "Streamflow in length cubed per time", "pkg": "Streamflow-Routine Package"},
".SRE": {"type": "CBC", "descr": "Sequential number of reaches", "pkg": "Streamflow-Routine Package"},
".SRO": {"type": "CBC", "descr": "Manning's roughness coefficient/C for each stream reach", "pkg": "Streamflow-Routine Package"},
".SSL": {"type": "CBC", "descr": "Streambed hydraulic conductance", "pkg": "Streamflow-Routine Package"},
".SST": {"type": "CBC", "descr": "Elevation of the top of the streambed", "pkg": "Streamflow-Routine Package"},
".STC": {"type": "CBC", "descr": "Slope of the stream channel in each reach", "pkg": "Streamflow-Routine Package"},
".STT": {"type": "CBC", "descr": " Stream stage", "pkg": "Streamflow-Routine Package"},
".SWI": {"type": "CBC", "descr": "Width of the stream channel in each reach", "pkg": "Streamflow-Routine Package"},
".298": {"type": "CBC", "descr": "Parameter numbers associated with SRF cells", "pkg": "Streamflow-Routine Package"},
".CH1": {"type": "CBC", "descr": "A non-zero value indicates a CHD cell", "pkg": "Time Variant Specified Head (CHD) Package"},
".CH2": {"type": "CBC", "descr": "Head at the beginning of a stress period (Start head)", "pkg": "Time Variant Specified Head (CHD) Package"},
".CH3": {"type": "CBC", "descr": "Head at the end of a stress period (End head)", "pkg": "Time Variant Specified Head (CHD) Package"},
".WEL": {"type": "CBC", "descr": "Volumetric recharge rate of wells", "pkg": "Well Package"},
".299": {"type": "CBC", "descr": "Parameter numbers associated with WELL cells", "pkg": "Well pkg"},
".ADV": {"type": "ASCII", "descr": "User-specified settings for the Advection Package", "pkg": "MT3D - Advection Package"},
".DPS": {"type": "ASCII", "descr": "User-specified settings for the Dispersion Package", "pkg": "MT3D - Dispersion Package"},
".TAL": {"type": "CBC", "descr": "Longitudinal dispersivity", "pkg": "MT3D - Dispersion Package"},
".CHE": {"type": "ASCII", "descr": "User-specified settings for the Chemical Reaction Package", "pkg": "MT3D - Chemical Reaction Package"},
".C91": {"type": "CBC", "descr": "Bulk density (only used by MT3D96)", "pkg": "MT3D - Chemical Reaction Package"},
".C92": {"type": "CBC", "descr": "First sorption constant (only used by MT3D96)", "pkg": "MT3D - Chemical Reaction Package"},
".C93": {"type": "CBC", "descr": "Second sorption constant (only used by MT3D96)", "pkg": "MT3D - Chemical Reaction Package"},
".C94": {"type": "CBC", "descr": "First-order rate constant for the dissolved phase (only used by MT3D96)", "pkg": "MT3D - Chemical Reaction Package"},
".C95": {"type": "CBC", "descr": "First-order rate constant for the sorbed phase (only used by MT3D96)", "pkg": "MT3D - Chemical Reaction Package"},
".TCH": {"type": "CBC", "descr": "Specified concentration at constant head cells.", "pkg": "MT3D - Sink & Source Mixing Package"},
".TE" : {"type": "CBC", "descr": "Specified concentration of evapotranspiration flux", "pkg": "MT3D - Sink & Source Mixing Package"},
".TG" : {"type": "CBC", "descr": "Specified concentration at general-head boundary cells", "pkg": "MT3D - Sink & Source Mixing Package"},
".TR" : {"type": "CBC", "descr": "Specified concentration of river", "pkg": "MT3D - Sink & Source Mixing Package"},
".TRC": {"type": "CBC", "descr": "Specified concentration of recharge flux", "pkg": "MT3D - Sink & Source Mixing Package"},
".TST": {"type": "CBC", "descr": "Specified concentration of stream", "pkg": "MT3D - Sink & Source Mixing Package"},
".TW" : {"type": "CBC", "descr": "Specified concentration of injection wells", "pkg": "MT3D - Sink & Source Mixing Package"},
".C54": {"type": "CBC", "descr": "Flag indicates a Time-variant specified concentration", "pkg": "MT3D - Sink & Source Mixing Package"},
".C55": {"type": "CBC", "descr": "Concentration in a Time-variant specified concentration cell", "pkg": "MT3D - Sink & Source Mixing Package"},
".PM5": {"type": "ASCII", "descr": "Most options and settings of a model are saved in this file", "pkg": "Other Reserved File Extensions"},
".L"  : {"type": "ASCII", "descr": "Settings of the Layer options", "pkg": "Other Reserved File Extensions"},
".GRD": {"type": "ASCII", "descr": "Grid Specification file (see Appendix 2 for the format)", "pkg": "Other Reserved File Extensions"},
".C97": {"type": "CBC", "descr": "Digitizer", "pkg": "Other Reserved File Extensions"},
"._83": {"type": "CBC", "descr": "Presentation", "pkg": "Other Reserved File Extensions"},
".WBL": {"type": "CBC", "descr": "subregions for the calculation of water budget", "pkg": "Other Reserved File Extensions"},
".POL": {"type": "BINARY", "descr": "contains boreholes and observations (saved automatically by PMWIN)", "pkg": "Other Reserved File Extensions"},
".PPL": {"type": "BINARY", "descr": "parameter list for PEST (saved automatically by PMWIN)", "pkg": "Other Reserved File Extensions"},
".UPL": {"type": "BINARY", "descr": "parameter list for UCODE (saved automatically by PMWIN)", "pkg": "Other Reserved File Extensions"},
".TRN": {"type": "BINARY", "descr": "is a time parameter file (saved automatically by PMWIN)", "pkg": "Other Reserved File Extensions"},
".TRS": {"type": "BINARY", "descr": "is a Trace file saved automatically by PMWIN", "pkg": "Other Reserved File Extensions"},
}

# %%

def get_pmwin_file_ext(path, pmwin_ftype='cbc'):
    """Return available file extensions in path pertaining to PMWIN files of given pmwin_ftype.
    
    Parameters
    ----------
    path: str
        The path to the file pmwin files directory.
    type : str
        one of LEGAL_PMWIN_TYPES i.e. oneof ('CBC', 'ASCII', 'BINARY', 'ZONE')

    Returns
    -------
    list of file extensions in path of PMMIN file types that are defined in the PMWIN book (2003)

    Examples
    --------
    present_cbc_files_ext = get_pmwin_files(path, 'cbc')
    present_binary_file_ext = get_pmwin_files(path, 'binary')
    present_ascii_file_ext = get_pmw_files(path, 'ascii')
    """
    if (not isinstance(pmwin_ftype, str)) or (not pmwin_ftype.upper() in LEGAL_PMWIN_TYPES):
        raise ValueError("Second argument, pmwin_ftype, must be one of {}".format(LEGAL_PMWIN_TYPES))

    pmwin_ftype = pmwin_ftype.upper()

    if pmwin_ftype == 'ZONE':
        warn("pmwin_ftype 'ZONE' not implemented.")
        return []

    legal_file_ext = [k for k in pkgs if pkgs[k]['type'] == pmwin_ftype]

    all_file_ext_in_path = [os.path.splitext(f)[1].upper() for f in os.listdir(path)]
    available_file_ext_of_ftype = set(legal_file_ext).intersection(all_file_ext_in_path)
    return available_file_ext_of_ftype


def get_pmwin_files(path, basename=None, pmwin_ftype='cbc'):
    """Return available files in path with extensions pertaining to PMWIN files of given pmwin_ftype.
    
    Parameters
    ----------
    path: str
        The path to the file pmwin files directory.
    type : str
        one of LEGAL_PMWIN_TYPES i.e. oneof ('CBC', 'ASCII', 'BINARY', 'ZONE')
    
        Note: 'ZONE' not implemented.

    Returns
    -------
    list of files 'basename.ext' in path of PMMIN file types that are defined in the PMWIN book (2003)

    Examples
    --------
    present_cbc_files = get_pmwin_files(path, 'cbc') # 'cbc' is case independent.
    present_binary_files = get_pmwin_files(path, 'binary')
    present_ascii_files = get_pmw_files(path, 'ascii')
    """
    if (not isinstance(pmwin_ftype, str)) or (not pmwin_ftype.upper() in LEGAL_PMWIN_TYPES):
        raise ValueError("Second argument, pmwin_ftype, must be one of {}.".format(LEGAL_PMWIN_TYPES))
    
    pmwin_ftype = pmwin_ftype.upper()
    
    if pmwin_ftype == 'ZONE':
        warn("pmwin_ftype 'ZONE' not implemented.")
        return []

    # Get legal file extensions from the dict of pkgs.
    legal_exts = set([k for k in pkgs if pkgs[k]['type'] == pmwin_ftype])

    # Get the present pmwin file basenames with a legal pmwin extension for files of given pmwin_ftype
    present_files_of_given_pmwin_ftype = [os.path.basename(f) for f in os.listdir(path)
            if os.path.splitext(os.path.basename(f))[1].upper() in legal_exts]
    return present_files_of_given_pmwin_ftype


get_cbc_files = lambda path, basename: get_pmwin_files(path, basename,'cbc')
get_binary_files = lambda path, basename: get_pmwin_files(path, basename, 'binary')
get_ascii_files = lambda path, basename: get_pmwin_files(path, basename, 'ascii')
get_zone_files = lambda path, basename: get_pmwin_files((path, basename, 'zone'))


def get_cbc_data(fname, shape=None, verbose=False, struct_type='f'):
    """Return the contents of a PMWIN cell-by-cell (CBC) file using dtype according to type in struct module.
    
    Parameters
    ----------
    fname : str
        name (local or full path) of PMWIN cell-by-cell (CBC)file
    shape : tuple of 3 ints (layer, row, col) of Modflow's model grid.
        shape of the 3D model grid
    verbose : boolean
        yields more info when function rns
    struct_type: single character
        tells which format and size the binary data in the file has (see module struct)
    """
    struct_types = struct_formats.keys()
    if not struct_type in struct_types:
        raise ValueError("type {} must be one of the type in module struct: []".
        format(struct_type, ",".join(struct_types)))

    # according to PMWIN.COM header consists of 1000x4 bytes (to be skipped)
    hdr_fmt = '1000i'

    # set format str that struct.unpack() understands
    lay_fmt = '{:d}{}'.format(np.prod(shape), struct_type)
    
    with open(fname, 'rb') as fp:
        hdr_tuple   = struct.unpack(hdr_fmt, fp.read(struct.calcsize(hdr_fmt)))
        layer_tuple = struct.unpack(lay_fmt, fp.read(struct.calcsize(lay_fmt)))
    if verbose:
        print(hdr_tuple) # if just curious to see what's in it (proves that it has no useful info).
    return np.array(layer_tuple).reshape(shape)

def get_cbc_files_with_non_unique_data(path, basename=None, shape=None, verbose=False):
    """Return list of pmwin cbc files with useful info in them
    
    Parameters
    ----------
    path: str
        path to the folder with the pmwin files.
    basename: str
        basename (without extension) of files to consider.
    shape: 3 tuple of ints
        (layer row col) of model grid.
    verbose: bool
        show more or less info

    Returns
    -------
    list of files (basename + extension)
    """
    cbc_files = get_pmwin_files(path, basename=basename, pmwin_ftype='cbc')
    useful_files = []
    for fname in cbc_files:
        if len(np.unique(get_cbc_data(os.path.join(path, fname), shape=shape, verbose=verbose))) > 1:
            useful_files.append(fname)
    return useful_files


# %% TODO Still experimental

def get_zone_data(fname, shape=None, verbose=False):
    """Return data in PMWIN zone file.
    
    TODO: this sill does not work, don't know the structure of the binay zone file. 
    @TO 20210716    
    """
    hdr_fmt = '18s'
    initial_bytes = 18 # to be tried out
    with open(fname, 'rb') as fp:
        hdr = struct.unpack(hdr_fmt, fp.read(struct.calcsize(hdr_fmt)))
        if verbose: print("Hdr = ", hdr)
        n_bytes = int(fp.seek(0, 2)) # file length
        if verbose: print('File length = {} bytes.'.format(n_bytes))
        fmt='{:d}h'.format((n_bytes - initial_bytes) // 2)
        fp.seek(initial_bytes, 0) # rewind
        zoneNrs= struct.unpack(fmt, fp.read(struct.calcsize(fmt)))
    return zoneNrs

def get_par_nrs(fname, shape=None, verbose=False):
    """Return paramter numbers in PMWIN file (which extensions?).
    
    TODO: this sill does not work, don't know the structure of the binay zone file. 
    @TO 20210716    
    """

    initial_bytes = 2000 #tried out
    with open(fname, 'rb') as fp:
        n_bytes = int(fp.seek(0, 2)) # file length
        if verbose: print('File length = {} bytes.'.format(n_bytes))
        fmt='{:d}i'.format((n_bytes - initial_bytes) // 4)
        fp.seek(initial_bytes, 0) # rewind
        parNrs= struct.unpack(fmt, fp.read(struct.calcsize(fmt)))
    return parNrs

# %% 

def get_obs_data(fname, verbose=False):
    """Return contents of binary observationf ile (extension 'pol').

    Ths is a trial to decipher its binary contents.
    
    Still no success.
    
    TO 20210716
    """
    hdr_fmt = '18sH'
    nobs_fmt = 'h'
    rec_fmt = '8c18c'
    with open(fname, 'rb') as fp:
        flen = fp.seek(0, 2) # rewind
        fp.seek(0, 0)
        hdr = struct.unpack(hdr_fmt  , fp.read(struct.calcsize(hdr_fmt)))
        nobs = struct.unpack(nobs_fmt, fp.read(struct.calcsize(nobs_fmt)))
        print(hdr)
        print('nobs = ', nobs)
        obs = []
        for i in range(200):
            obs.append(struct.unpack(rec_fmt, fp.read(26)))
            #obs.append(struct.unpack('d', struct.unpack(rec_fmt, fp.read(26))[0][-8:]))
        if verbose:
             print("Hdr = ", hdr)
    return obs

# %% Read ovservation data directly into a numpy array (alternative to above).

# This works well but does not give useful information.

# fname = os.path.join('/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/Yasin_1999_Eastern_Basin/', 'pm5_1_model_files/trial2.pol')

# with open(fname, 'rb') as fp:
#     n = 10  # read 10 observations
#     buf = fp.read(20 + 26 * n) # total numnber of bytes to read from file
#     obs  = np.frombuffer(buf, dtype='S18', count=1, offset=0)  # the text at the start of the file
#     nobs = np.frombuffer(buf, dtype='i2',  count=1, offset=18) # this (I suppose) should be NOBS?
#     # Then we read n observations starting at byte 20 (skipping first 20 bytes)  
#     data = np.frombuffer(buf,
#         dtype=[('nr', 'S8'), ('data', 'S10'), ('value', 'f8')],
#         count=n, offset=20)

# print('obs:', obs)
# print('nobs:', nobs)
# print(data)

# %%
def main():
    path = os.path.join('/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/Yasin_1999_Eastern_Basin/', 'pm5_1_model_files/')

    basename = 'trial2'

    shape = (4, 98, 47)

    for pmw_ext in LEGAL_PMWIN_TYPES:
        print()
        print("PMWIN files of pmwin_type {}:".format(pmw_ext))
        print(get_pmwin_file_ext(path, pmw_ext))
        print()
        print("Actual files of pmwin type {}:".format(pmw_ext))
        print(get_pmwin_files(path, basename, pmw_ext))
        print()

    print("Struct_format keys with their standard size (note -1 means 'size is not fixed'):")
    for k in struct_formats:
        print("{}, {:2d}".format(k, struct_formats[k]['standard size']))

    # %% The the info of the 3D array associated with a given extension (with or without dot, any case)

    #  Choose an extension of a cbc files and check to see if the array is read properly.
    # These may work (are cbc files in the Yasin pmwin files directory:)
    # '.POR', '.66', '.298', '.291', '.TG', '.67', '.296', '.SST', '.IBD', '.TAL', '.68', '.GHC', '.SWI', '.292', '.DRC', '.HEA', '.DRE', '.C91', '.295', '.299', '.SRE', '.SEG', '.65', '.SBO', '.RCH', '.TOP', '.STT', '.TIC', '.64', '.SRO', '.CON', '.294', '.69', '._83', '.297', '.STC', '.LEA', '.RCI', '.63', '.293', '.DWA', '.TSC', '.SFL', '.WEL', '.GHB', '.SSL', '.BOT'

    # The resulting array values are always floats.

    # Fill in the letters of any extension and get the infor on the data (if ext exists)
    e = 'ibd' # a 3-letter extension any case (with or without the dot).

    # PMWIN stores CBC data always as floats.
    struct_type = 'f'  # if not e[0].lower() in ['i', 'j', 'k', 'l', 'm', 'n'] else 'i'

    print("Get the data of the file with extension `{}`:".format(e))

    ext = e.upper() if e.startswith('.') else ('.' + e).upper()

    print("Extension `{}` -> `{}` (pmwin type = `{}`) is the `{}` of of package `{}`".format(e, ext,
                                pkgs[ext]['type'],
                                pkgs[ext]['descr'],
                                pkgs[ext]['pkg'],
    ))

    # The actual data array
    arr = get_cbc_data(os.path.join(path, basename + ext), shape=shape, struct_type=struct_type)

    print("\nThe unique values in the array are:")
    print('The array has {:d} unique values:'.format(len(np.unique(arr))))
    print(np.unique(arr))

    if len(np.unique(arr)) > 1:
        print("\nThe array:")
        print(arr)
    else:
        print('Array not printed.')
    # %%
    useful_cbc_files = get_cbc_files_with_non_unique_data(path, basename=basename, shape=shape, verbose=False)
    print("Useful pmwin files of pmwin type 'cbc' are:")
    print(useful_cbc_files)
    # %%
    for fname in useful_cbc_files:
        d = get_cbc_data(os.path.join(path, fname), shape=shape)
        print(fname)
        print(np.unique(d))


print("\n__name__ = {}\n".format(__name__))

# %%
if __name__ == '__main__':
    print("Running main():")
    main()
    print("\nmain done")

