#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 19 00:12:31 2021

Eastern Mountain Aquifer model
Raslan Mohammed Yasin (1999) MSc Thesis H.H. 363, IHE Institute Delft

MsC Thesis Title:
Groundwater Modelling of Fractured-Karstic Environments:
A Case Study of the Eastern Aquifer Basin, Palestine

The model was sent to me by his Supervisor Drs. Jan Nonner in may 2021. The form
it was sent was as a set of PMWin 5 files.

This code reads in the original data into a dict, such that all data pertaining
to the original modle is contained in that dict.

In another python file, this dict is converted to the complete set of data
for a 3D mf6 model, which is no longer quai3D but full 3D, but still yields the
same outcomes.

Notice:
According to the report by Yasin (1999), the extent of the model is as follofolders['src'],
whih must by the old Israel EPGS:2039 projecttion:
150000 - 200000   # E-W
 85000 - 185000   # S-N

 Translated to the new Israeli/Palestinian EPG:6984S this becomes:
 2000000 - 250000 (adds  50000 to the old x-coordinates)
 5850000 - 685000 (adds 500000 to the old y-coordinates)

Notice further:
This EPGS info still does not completely define the extents of the model, because
its width and height are 47 and 98 cells of 1x1 km in size. Therefore, we stillneed
the information of the required shift in both horizontal x as in the horizontal y direction that we cannot obtain from the Thesis text. We, therefore, have to figure
that out ourselves, which is not trivial due to the low resolution of the picture
in the old Thesis.

 So we must define a not yet defined offset `x_shift` and `y_hift` for the model grid
 and add a shift accordingly to the coordinates layer, which we may have to
 adjust layer map.

@author: Theo
"""

__all__ = 'yasin_old'

# %%
import logging
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import re
import pickle

def whoami():
    return sys._getframe(2).f_code.co_name

def calling():
    str = "calling " + whoami()
    print(str)
    #print(len(str) * '=')

from setPaths import folders
import pmwinBinaryFiles as pb

from fdm.mfgrid import Grid
from etc import newfig

logging.disable(sys.maxsize) # disable loging

# %% Coordinate systems and upper left point of of the model network.

# The old projection used by Yasin (1999)
proj = 'EPGS:2039' # Old map projection.
xW_old = 150000 # ul of model in new EPGS6984 
yN_old = 185000 # ul of model in new EPGS6984

# The new projection (Israel - Palestine national projection)
# upper left corner of model in EPGS6984, new map projection
DX_P2EPGS2039 =  50000 # shift from old israel coords to new (EPGS6984)
DY_P2EPGS2039 = 500000 # shift from old israel coords to new (EPGS6984)
# Shift required beause Yasin's model is 3 km short EW and 2 km short NS relative
# to the size of the area hy defineds in his MsC report
x_shift = 0 # May be 0, 1000, 2000 or 3000 (model has 47 columns of 1 km)
y_shift = -2000 # may be 0, -1000 or -2000  (model has 98 rofolders['src'] of 1 km)

# Upper left and upper right coordinate of Yasin (1999) model in the new
# Israel-Palestine coordinate system EPGS:6984.
proj = 'EPGS:6984'
xUL = xW_old + DX_P2EPGS2039 + x_shift
yUL = yN_old + DY_P2EPGS2039 + y_shift 

# Relevant pmwin files, i.e. the ones used by the original model
pmfiles = ['bas.dat', 'bcf.dat', 'ghb.dat', 'oc.dat', 'rch.dat', 'wel.dat']

basename = 'trial2'

# Notice that all other files in this directory are PMW-generated files, not
# used-defined files. These PMWi-generated files all have basename `trial2` with
# extensions defined in the book by Chiang and Kinzelbach (2003) on PMWIN. These
# files have been veried for the compatibility with the *.dat files. They match
# the same model.

# %% Functions used to read portions of the data files

# A series of functions were crafted to read in the original dat files that define
# the original Yasin(1999) model. The model was made in MOMDFLOW1996 and was
# steady-state, quasi 3D with 4 aquifers, separated by aquitards defined by
# the VCONT (vertical leakance) parameter.

def getFtnFmtTpl(rec):
    """Return a 3 tuple holding the fortran format.

    The format will be (n [IGEe] (m, m))
    """
    regex = re.compile(r'.?\((\d+)([eEIFG])(\d+\.\d+|\d+)')
    A = regex.findall(rec)[0]
    return int(A[0]), A[1], tuple([int(n) for n in A[2].split('.')])


def getRecTokens(rec, verbose=None):
    """Return a record of tkons (words)."""
    regex = re.compile(r'\w+')
    A = regex.findall(rec)
    if verbose: print(A)
    return A


def getInts(rec, verbose=None):
    """Return a record of integer numbers in the line."""
    regex = re.compile(r'[+-]?\d+')
    I = [int(a) for a in regex.findall(rec)]
    if verbose: print(I)
    return I


def getFloats(rec, verbose=None):
    """Return a record of floating point numbers in string."""
    regex = re.compile(r'[+-]?\d*\.?\d+[eE][-+]?\d+|[+-]?\d*\.?\d+')
    A = [float(a) for a in regex.findall(rec)]
    if verbose: print(A)
    return A


def readAlist(fp, N=None, verbose=None):
    """Read a list of length N of data including its format line.
    
    Using only for reading bcf TPRY, DELR and DELC data.
    """
    fmt = getFtnFmtTpl(fp.readline())
    if verbose:
        print(fmt)
    getRecTokens = getInts if fmt[1] == 'I' else getFloats
    dtype = int if fmt[1] == 'I' else float
    n = 0
    A = []
    while n < N:
        res = getRecTokens(fp.readline())
        A += res
        n += len(res)
    return np.array(A, dtype=dtype)


def read2DLayer(fp, nrow, ncol, verbose=None):
    """Return 2D array of a single layer including its format line."""
    fmt = getFtnFmtTpl(fp.readline())
    if verbose:
        print(fmt)
    getRecTokens = getInts if fmt[1] == 'I' else getFloats
    dtype = int if fmt[1] == 'I' else float
    N = 0
    A = []
    while N < nrow * ncol:
        res = getRecTokens(fp.readline())
        A += res
        N += len(res)
    return np.array(A, dtype=dtype).reshape((nrow, ncol))


def readLayers3D(fp, nlay, nrow, ncol, verbose=None):
    """Return 3D array of size nlay, nrow, ncol."""
    A = []
    for ilay in range(nlay):
        if verbose:
            print("Layer {:2}".format(ilay))
        A.append(read2DLayer(fp, nrow, ncol))
    return np.array(A)


def getListOfFLowPkgs(fp):
    """Return the list data of flow packages WEL, GHB, RIV, DRN, CHD."""
    n = getInts(fp.readline())[0] # number of records in the file
    data = []
    for i in range(n):
        s = fp.readline()
        loc = tuple(getInts(s)[:3])  # one-base because reada from dat file
        loc = tuple([l - 1 for l in loc]) # zero-based
        dta = tuple(getFloats(s)[3:])
        data.append((loc, *dta))
    return data


def readBas(fname):
    """"Read the input fo the 'bas' package.

    Parameters
    ----------
    fname: str
        path to the bas data file (text file).
    """
    with open(fname, 'r') as fp:
        bas = {}
        _ = fp.readline()
        _ = fp.readline()
        (bas['nlay'], bas['nrow'], bas['ncol'],
         bas['NPER'], bas['ITUNI']) = getInts(fp.readline())
        _ = fp.readline()
        _ = fp.readline()

        bas['IBOUND'] = readLayers3D(
            fp, bas['nlay'], bas['nrow'], bas['ncol'], verbose=False)
        bas['HNOFLO'] = getFloats(fp.readline())[0]
        bas['STRT'] = readLayers3D(
            fp, bas['nlay'], bas['nrow'], bas['ncol'], verbose=False)
        bas['PERLEN'], NSTP, bas['TSMULT'] = getFloats(fp.readline())
        bas['NSTP'] = int(NSTP)
        return bas


def readBcf(fname, bas, basename='trial2'):
    """"Read the input for the 'bcf' package.

    Parameters
    ----------
    fname: str
        path to the bas data file (text file).
    """
    nlay, nrow, ncol, nper, nstp = (bas['nlay'], bas['nrow'],
                                    bas['ncol'], bas['NPER'], bas['NSTP'])
    shape = nlay, nrow, ncol

    bcf = dict(nlay=nlay, nrow=nrow, ncol=ncol, nper=nper, nstp=nstp)

    with open(fname, 'r') as fp:
        rec = fp.readline()
        (_, bcf['IBCFCB'], bcf['HDRY'], bcf['IWDFLG'], bcf[' WETFCT'],
            bcf['IWETIT'], bcf['IHEWET']) = getFloats(rec)
        for k in bcf:
            if k[0].upper() in 'IJKLNM':
                bcf[k] = int(bcf[k])

        bcf['LTYPE'] = getInts(fp.readline())[:nlay]

        # TRPY ansiotropy
        bcf['TPRY'] = readAlist(fp, N=nlay)
        bcf['DELR'] = readAlist(fp, N=ncol)
        bcf['DELC'] = readAlist(fp, N=nrow)

        bcf['HY']    = np.zeros(shape) # Hydraulic conductivity (Kx)
        bcf['BOT']   = np.zeros(shape) # Bottom of layers [0:]
        bcf['TOP']   = np.zeros(shape) # Top lo layers    [1:]
        bcf['VCONT'] = np.zeros(shape)[:-1] # Vertical leakance [1/d] [0:-1]

        # layer 1
        # Layer [0] does not have a top because phreatic layer was used.
        #  So here still zeros. Use bcf['BOT'][0] + 100 as default for now.
        for iL in range(shape[0]):
            bcf['HY' ][iL]   = readLayers3D(fp, 1, nrow, ncol, verbose=False)
            bcf['BOT'][iL]   = readLayers3D(fp, 1, nrow, ncol, verbose=False)
            if iL < shape[0] - 1:
                bcf['VCONT'][iL] = readLayers3D(fp, 1, nrow, ncol, verbose=False)
            if iL == 0:
                bcf['TOP'][iL]   = bcf['BOT'][0] + 100 # Aribtrary, to be replaced by DEM
            else:
                bcf['TOP'][iL]   = readLayers3D(fp, 1, nrow, ncol, verbose=False)

        bcf['LEA'] = pb.get_cbc_data(os.path.join(folders['pmwin'], basename + '.lea'),
                                    shape=shape)

        # Uses global values for xUL and yUL (in new Israel-Palestine coordinates [m])
        # Hence proj = EPGS:6984
        proj = 'EPGS:6984'
        x = xUL + np.hstack((0, np.cumsum(bcf['DELR'])))
        y = yUL - np.hstack((0, np.cumsum(bcf['DELC'])))

        # Z has 1 more than the number of layers
        Z = np.zeros((nlay + 1, nrow, ncol))
        Z[0]  = bcf['TOP'][0]  # top[0] all zeros, actual values filled in later
        Z[1:] = bcf['BOT']

        # The old model grid is captured here in a gr object and added to bcf dict
        bcf['gr']   = Grid(x, y, Z)
        bcf['proj'] = proj
        return bcf


def readRch(fname, bas):
    """"Read the input fo the 'rch' package.

    Parameters
    ----------
    fname: str
        path to the bas data file (text file).
    """
    _, nrow, ncol = bas['nlay'], bas['nrow'], bas['ncol']

    iper = 0
    rch = dict(nrow=nrow, ncol=ncol)
    with open(fname, 'r') as fp:
        _ = fp.readline()
        _ = fp.readline()
        rch['RECH'] = {iper: readLayers3D(
            fp, 1, nrow, ncol, verbose=False)[0]}      # single 2D array
        return rch


def readWel(fname):
    """Read wel data."""
    wel = dict()
    iper = 0
    with open(fname, 'r') as fp:
        _ = fp.readline()
        wel[iper] = getListOfFLowPkgs(fp)
    return wel


def readGhb(fname):
    """Read ghb data."""
    ghb = dict()
    iper = 0
    with open(fname, 'r') as fp:
        _ = fp.readline()
        ghb[iper] = getListOfFLowPkgs(fp)
    return ghb


def readOc(fname, bas):
    """Read oc data."""
    with open(fname, 'r') as fp:
        # for each simulation
        oc = {}
        oc['IHEDFM'], oc['IDDNFM'], oc[' IHEDUN'], oc['IDDNUN'] = getInts(
            fp.readline())
        oc['data'] = []
        for iper in range(bas['NPER']):
            for stp in range(bas['NSTP']):
                # For each time step
                # INCODE IHDDFL IBUDFL ICBCFL
                rec = getInts(fp.readline())
                oc['data'].append(rec)
                incode = rec[0]
                nl = 0 if incode < 0 else 1 if incode == 0 else bas['nlay']
                for ilay in range(nl):
                    # Hdpr Ddpr Hdsv Ddsv
                    oc['data'].append(getFloats(fp.readline()))
    return oc


def getChdData(bas=None, bcf=None):
    """Return chd period data from ibound and strt."""
    gr = bcf['gr']
    iper = 0
    LRC = gr.LRC(bas['IBOUND'] < 0) # note this is zero-based
    heads = bas['STRT'][bas['IBOUND'] < 0]
    return {iper: [(tuple(lrc), h) for lrc, h in zip(LRC, heads)]}


# %% Read all Yasin's 1999 model data

def read_old_model(folders, basename=None):
    """Return dict with all data pertaining to the old Yasin 1999 quasi 3D model.py
    
    The original Yasin (1999) model is a steady-state quasi-3D model Modflow96
    model consisting of with 4 layers, 98 rofolders['src'] and 47 columns with cells of
    1 km x 1 km in size.

    The result of this function is a dict of which the keys reference
    the flow packages used in the original model and of which each
    record is a dict itself specifying arrays or joining data used
    within each of the packages.

    This dict will be used by subsequent python file to convert the original model
    into a fully 3D model.
    """
    calling()

    os.chdir(folders['src']) # chdir to workspace

    yasin_old = {}
    bas = readBas(os.path.join(folders['pmwin'], 'bas.dat'))
    bcf = readBcf(os.path.join(folders['pmwin'], 'bcf.dat'), bas, basename=basename)
    wel = readWel(os.path.join(folders['pmwin'], 'wel.dat'))
    ghb = readGhb(os.path.join(folders['pmwin'], 'ghb.dat'))
    oc  = readOc(os.path.join(folders['pmwin'],  'oc.dat'), bas)
    rch = readRch(os.path.join(folders['pmwin'], 'rch.dat'), bas)
    chd = getChdData(bas=bas, bcf=bcf)

    yasin_old['bas'] = bas
    yasin_old['bcf'] = bcf
    yasin_old['wel'] = wel
    yasin_old['ghb'] = ghb
    yasin_old['oc']  = oc
    yasin_old['rch'] = rch
    yasin_old['chd'] = chd
    print('dict yasin_old returned')
    return yasin_old


def verify_k_vs_lea(yasin_old, folders=None, basename=None):
    """Verify conductivity with the basename.lea file.

    Parameters
    ----------
    yasin_old: dict
        data containd in the input *.dat files of the Yain 1999 original model.
    folders['pmwin']: str
        path to directory with the original pmwin data files.
    basename: str
        basename of the files that pmwin generated (name of the model)
    
    Some computations with the horizontal and vertical conductances HE and LEA.
    """
    calling()

    gr = yasin_old['bcf']['gr']
    VCONT = yasin_old['bcf']['VCONT']
    HY = yasin_old['bcf']['HY']
    fpath = os.path.join(folders['pmwin'], basename + '.lea')
    LEA = pb.get_cbc_data(fpath, shape=gr.shape)

    print('Showing unique HY and LEA and VCONT values and plotting HY LEA and VCONT.')
    with np.printoptions(precision=3, suppress=True, edgeitems=None):
        for iL in range(4):
            print('Layer {}'.format(iL))
            print("\nUnique HY[{}] values (n = {}):\n{}".format(iL, len(np.unique(HY[iL])), np.unique(HY[iL])))
            print("\nUnique LEA[{}] values (n = {}):\n{}".format(iL, len(np.unique(LEA[iL])), np.unique(LEA[iL])))
            print("Unique HY/LEA[{}] values (n = {}):\n{}".format(iL, len(np.unique(HY[iL]/LEA[iL])), np.unique(HY[iL]/LEA[iL])))
            print('\nPlot HE, LEA and VCONT for layer {}'.format(iL))
            fig, axs = plt.subplots(1, 3, sharex=True, sharey=True)
            for ia, (ttl, arr) in enumerate(zip(['HE', 'LEA', 'VCONT'], [HY, LEA, VCONT])):
                axs[ia].set_title(ttl + str(iL))
                axs[ia].set_xlabel('x')
                axs[ia].set_ylabel('y')
                axs[ia].grid(True)             
                axs[ia].imshow(np.log10(arr[ia]))

        print('\nOverall for LEA:'),
        print('LEA, shape ={}'.format(LEA.shape))
        print('LEA has {} unique values:\n{}'.format(
            len(np.unique(LEA)), np.unique(LEA)))
        print()
        print(LEA)
    print('Done.')


def verify_old_files(folders=None, basename='trial2', yasin_old=None):
    """Return result of comparison of  *.dat files with basename.* files
    
    The data files for the model are *.dat files. Yet PMWIN spefolders['src'] out a series of
    files with the same basename and varous extensions of which the meaning is
    described in the book Chiang & Kinzelbach (2003) on PMWIN. Here we verify
    if the contents of the *.dat files match those of the basename.* files.
    We do this only for relevant files, as many of the basename.* files do not
    contain data used by the model or data at all.

    The relevant extensions are:
     '.top': top of the model layers
     '.bot': bottom of the model layers
     '.con': horizontal conductivity of the model layers
     '.lea': vertical conductances of layers (not leakances)
    """
    calling()

    msg = []
    gr = yasin_old['bcf']['gr']
    
    # Verify top of layers
    fpath   = os.path.join(folders['pmwin'], basename + '.top')
    top_old = pb.get_cbc_data(fpath, shape=gr.shape)
    if np.all(np.isclose(gr.Z[1:-1], top_old[1:])):
        msg.append("Top in bcf.dat same as in trial2.top, so all ok.")
    else:
        msg.append("Top in bcf.dat not same as in trial2.top, not ok !!!.")

    # Verify bottom of layers
    fpath = os.path.join(folders['pmwin'], basename + '.bot')   
    bot_old = pb.get_cbc_data(fpath, shape=gr.shape)
    if np.all(np.isclose(gr.Z[1:], bot_old)):
        msg.append("Bot in bcf.dat same as in trial2.bot, so all ok")
    else:
        msg.append("Bot in bcf.dat not the same as in trial2.bot, not ok !!!")
    
    # Verify conductivities of layers
    fpath = os.path.join(folders['pmwin'], basename + '.con')   
    HY    = pb.get_cbc_data(fpath, shape=gr.shape)
    if np.all(np.isclose(HY, yasin_old['bcf']['HY'])):
        msg.append("HY (kh) in bcf is the same as in 'trial2.con")
    else:
        msg.append("HY (kh) in bcf is not the same as in 'trial2.con', so not ok !!!")
    for m in msg:
        print(m)
    print('Done.')

def show_the_old_model(yasin_old):
    """Show the elevations and conductivities of the old Yasin 1999 model in plots.
    """
    calling()

    gr = yasin_old['bcf']['gr']
    
    print('Showing elevations of the base of the 4 layer (gr.Z[1:]), See plots')
    levels = np.arange(-1200, 1200, 100)
    for iz in range(gr.nz + 1):
        ax = newfig("Elevation base of layer iz={}".format(iz),
                            'x EPGS6984', 'y EPGS6984', aspect=1.0)
        c = ax.contourf(gr.xm, gr.ym, gr.Z[iz], levels=levels, cmap='viridis')
        cb = plt.colorbar(c)
        cb.ax.set_title("Elevation [m]")

    print('Showing log10 conductivities (HY). `see plot')
    HY = yasin_old['bcf']['HY']
    lgK = HY
    levels = np.arange(np.floor(np.min(lgK)), np.ceil(np.max(lgK)), 0.1)

    for iz in range(gr.nz):
        ax = newfig("log10(k) iz={}".format(iz),
                 'x EPGS6984', 'y EPGS6984', aspect=1.0)
        lgk = np.log10(HY[iz])
        levels = np.arange(np.floor(np.min(lgK[iz])), np.ceil(np.max(lgK[iz])), 0.1)
        if len(levels) >= 2:
            c = ax.contourf(gr.xm, gr.ym, lgk, levels=levels, cmap='viridis')
            print('k_min = {:.4g}, k_max = {:.4g}'.format(np.min(lgk), np.max(lgk)))
            cb = plt.colorbar(c)
            cb.ax.set_title("log10(k)")
    print('Done')
    return None

def figure_out_lea(yasin_old, folders=None):
    """Return figure out the relation between HY, VCONT and trial2.lea.tex

    HY (hydraulic conductivity of the layers in BCF)
    VCONT (vertical leakance between the layers)
    trial.lea (vertical conductivity of the layers (acc. Chiang&K(2003)))
    """
    gr = yasin_old['bcf']['gr']
    HY = yasin_old['bcf']['HY']
    VCONT = yasin_old['bcf']['VCONT']
    LEA = yasin_old['bcf']['LEA'] # acc to Chiang&Kinzelbach(2003) = kv

    print("""Comments:
    LEA=kv makes sense when compared to HY=kh. Both have the same number of
    layers and both have a single value in layer 0 and layer 2, while HY > LEA.
    Also, the values in layers 1 and 3 make sense if related to kh.
    """)
    
    # Let's relate VCONT to the layer below the aquitard (which is no separate)
    # layer when using the BCF package
    # VCONT [1/D] = kv / D --> kv [m/D]= VCONT * d
    # VCONT [1/D] = kv / D --> c    [D]= d / kv = 1 / VCONT

    kv = np.zeros(gr.shape)
    kv[1:] = gr.DZ[1:] * VCONT
    # Vertical resistance computed from VCONT between two aquifers
    cvert1 = 1 / VCONT
    # Vertical resistance compute from LEA using both aquifer thicknesses
    cvert2 = 0.5 * (gr.DZ[:-1] / LEA[:-1] + gr.DZ[1:] / LEA[1:])

    print("""
    Comparing VCONT to LEA by computing the vertical resistances:
    comparing np.unique(cvert1) and np.unique(cvert2) yields"
    array([   85.75115871,   103.33333712,   103.42464868, ...,
       20101.04392762, 20233.41674222, 20519.97619683])
    array([   85.7511676 ,   103.33333184,   103.42465595, ...,
       12968.49295667, 14068.49294028, 14162.09396167])

    This shofolders['src'] both are essentially the same, there is only a factor of about
    two in the higher resistances. We can just plot the two next to each other.

    The difference may be due to the circumstance that the elevation of the
    top aquier (ground surface) was arbitrily set to 100 m above its bottom,
    because the original model does not have a top as it has a water table.
    """)
    m =  np.log10(np.round(min(np.min(cvert1), np.min(cvert2)), decimals=1))
    M =  np.log10(np.round(max(np.max(cvert1), np.max(cvert2)), decimals=1))
    levels =  np.arange(m, M + 0.1, 0.1)
    extent = gr.xm[0], gr.xm[-1], gr.ym[-11], gr.ym[0]
    for iL, (cv1, cv2) in enumerate(zip(cvert1, cvert2)):
        _, axs = plt.subplots(1, 2, sharex=True, sharey=True)
        axs[0].grid(True)
        axs[1].grid(True)
        axs[0].set_title('cvert1[{}-{}] from VCONT'.format(iL, iL + 1))
        axs[1].set_title('cvert2[{}-{}] from LEA'.format(iL, iL + 1))
        axs[0].set_xlabel('x [m]')
        axs[1].set_xlabel('x [m]')
        axs[1].set_ylabel('y [m]')
        axs[1].set_ylabel('y [m]')
        axs[0].imshow(np.log10(cv1), extent=extent, vmin=levels[0], vmax=levels[-1])
        axs[1].imshow(np.log10(cv2), extent=extent, vmin=levels[0], vmax=levels[-1])

    print("""
    How to proceed?:
    The best way to proceed seems to used the Lea as vertical conductivities.
    If it does not work we may choose to use the VCONT in this way:

    Furthermore, Yasin99 does not have a top of the top aquifer because that
    aquifer was phreatic. I arbitrily set it to 100 m above its bottom. A real
    top will be obtained from the DEM for yasin_mf6.
    """)

    kv = LEA.copy()
    for iL in [1, 2, 3]:
        kv[iL, :, :] = (2 * VCONT[iL -1 ] * gr.DZ[iL]
                    - kv[iL - 1] * gr.DZ[iL -1] / gr.DZ[iL]).copy()

    # Comparing cvert3 (from VCONT and LEA0 with that from LEA)
    cvert3 = np.log10(0.5 * (gr.DZ[:-1] / kv[:-1] + gr.DZ[1:] / kv[1:]))
    for iL, (cv1, cv3) in enumerate(zip(cvert1, cvert3)):
        _, axs = plt.subplots(1, 2, sharex=True, sharey=True)
        axs[0].grid(True)
        axs[1].grid(True)
        axs[0].set_title('cvert3[{}-{}] from VCONT'.format(iL, iL+1))
        axs[1].set_title('cvert2[{}-{}] from LEA'.format(iL, iL+1))
        axs[0].set_xlabel('x [m]')
        axs[1].set_xlabel('x [m]')
        axs[1].set_ylabel('y [m]')
        axs[1].set_ylabel('y [m]')
        axs[0].imshow(np.log10(cv3), extent=extent, vmin=levels[0], vmax=levels[-1])
        axs[1].imshow(np.log10(cv2), extent=extent, vmin=levels[0], vmax=levels[-1])

    print("""
    Plotting cvert3:
    This reveals that the vertical conductivity is not compatible with the resistance,
    since there is at points no postitive vertical conductivity possible when the
    VCONT is converted to vertical conductivities for all layers.
    Because cvert2 yields reasonable values for the vertical conductities, we will use these LEA as the vertical conductivities.
    """)
    return None

# Will be imported from this module    
yasin_old = read_old_model(folders, basename=basename)

def setup_old_model(yasin_old):
    """Setup a flopy model using the old Yassin input directly (quasi 3D)
    
    Parameters
    ----------
    yasin_old: dict
        the data of the original model in a dict.
    """

    pass

# %% Compare the well coordinates by comparing the report coordinates with those of the well input
if __name__ == '__main__':
    """Specific for testing"""

    with open(os.path.join(folders['test'], 'yasin_old.pickle'), 'wb') as fp:
        pickle.dump(yasin_old, fp)

    show_the_old_model(yasin_old)

    msg = verify_old_files(folders, basename='trial2', yasin_old=yasin_old)

    msg_leak = verify_k_vs_lea(yasin_old, folders=folders, basename=basename)

    figure_out_lea(yasin_old, folders=folders)  

    print("Done !")
    plt.show()

# %%

