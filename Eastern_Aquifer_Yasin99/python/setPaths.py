#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" 
# set_paths.py

# Small module to add directories to sys.path specific to this workspace
# This was a brilliant idea found in
# https://stackoverflow.com/questions/15208615/using-pth-files
"""

# %%
import sys
import os
import logging
logging.basicConfig(level=logging.WARNING, format=' %(asctime)s - %(levelname)s - %(message)s')

logging.warning("Importing setPaths --> see project folders, see `folders`")

#sys.path.append('/whatever/dir/you/want')
folders = dict()
folders['hygea'] = '/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/'
folders['proj'] = os.path.join(folders['hygea'], '2020_BUZA_ISR_PAL_T/')
folders['models'] = os.path.join(folders['proj'], 'mountain_aquifers/')
folders['model'] = os.path.join(folders['models'], 'Eastern_Aquifer_Yasin99/')
folders['src'] = os.path.join(folders['model'], 'python/')
folders['data'] = os.path.join(folders['models'], 'data/')
folders['dems'] = os.path.join(folders['data'], 'USGS_GMTED2010/gmted2010/')
folders['dem300'] = os.path.join(folders['dems'], 'GMTED2010N30E030_300/')
folders['pmwin'] = os.path.join(folders['model'], 'Yasin_pm51/')
folders['Bastiaanssen'] = os.path.join(folders['data'], 'Bastiaanssen/')
folders['src_python'] = folders['src']
folders['bast_python'] = os.path.join(folders['Bastiaanssen'], 'python')
folders['dems_python'] = os.path.join(folders['dems'], 'python/')
folders['rch'] = os.path.join(folders['Bastiaanssen'], 'GWB1_Groundwater_Recharge/')
folders['evt'] = os.path.join(folders['Bastiaanssen'], 'SWB9_Actual_Evapotranspiration_ETact/')
folders['sup'] = os.path.join(folders['Bastiaanssen'], 'GWB3_Groundwater_Supply/')
folders['tst'] = os.path.join(folders['Bastiaanssen'], 'tiftests/')
folders['rch_yearly' ] = os.path.join(folders['rch'], 'yearly/')
folders['rch_monthly'] = os.path.join(folders['rch'], 'monthly/')
folders['evt_yearly' ] = os.path.join(folders['evt'], 'yearly/')
folders['evt_monthly'] = os.path.join(folders['evt'], 'monthly/')
folders['sup_yearly' ] = os.path.join(folders['sup'], 'yearly/')
folders['sup_monthly'] = os.path.join(folders['sup'], 'monthly/')
folders['tst_monthly'] = os.path.join(folders['tst'], 'monthly/')
folders['test'] = os.path.join(folders['src'],'tests/')
folders['gis'] = os.path.join(folders['models'], 'QGIS/')
folders['exe'] = '/Users/Theo/GRWMODELS/mfLab/trunk/bin/'
folders['tools'] = '/Users/Theo/GRWMODELS/python/tools/'
folders['exe'] = '/Users/Theo/GRWMODELS/mfLab/trunk/bin/'

for key in ['src_python', 'dems_python', 'bast_python', 'tools']:
    if not folders[key] in sys.path:
        sys.path.append(folders[key])
        
gr_surface_tif = os.path.join(folders['dems'], 'mdl_elev.tif')

os.chdir(folders['src_python'])
logging.warning("cwd = {}".format(os.getcwd()))

if __name__ == '__main__':
    # Show the folders (They have already bieen asserted).
    mxlen = 80
    print("\nFolders used, truncated if path len is > {} :".format(mxlen))
    for f in folders:
        fold_name = folders[f] if len(folders[f]) < mxlen + 3 else '...' + folders[f][-mxlen:]
        print(f"{f:12s} :, {fold_name}")
        assert os.path.isdir(folders[f]), f'No such folder: {fold_name}' 
        
    print("\nChecked: All folders in dict 'folders' do exist.\n")

# %%
sys.version
# %%
