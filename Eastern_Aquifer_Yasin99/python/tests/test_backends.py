# %%
import matplotlib.pyplot as plt
import numpy as np


print("The current backend is: {}".format(plt.get_backend()))

#%%

x = np.linspace(0, 2 * np.pi, 100)
y = np.sin(x) * np.cos(x)
plt.plot(x, y)
plt.grid(True)

print("The current backend is: {}".format(plt.get_backend()))
plt.show()

print("The current backend is: {}".format(plt.get_backend()))