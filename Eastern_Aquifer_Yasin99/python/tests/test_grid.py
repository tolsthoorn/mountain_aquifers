"""
Verify the sanity of the grid.

1) Does the grid have any active cells with (almost) zero thickness?
2) Are there wells in cells with almost zero thickness?

First check for the original model (which is capped with a top surface that was
arbitrerily set to 100 m above the bottom of the top aquifer, because no top
was given for this water-table aquifer)

Then check the new grid, which is the same as the old one, but with the
dem placed on top of it.


@author: Theo 2021-13-09
"""
# %%
#import inspect
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
import rasterio
from etc import newfig, color_cycler, line_cycler
from fdm.mfgrid import Grid
#from yasin_old_model import yasin_old
#import analyze_ims_convergence as conv_test
import rasterio
import pandas as pd
import pickle
import warnings
import unittest
import pickle

src_dir = ('/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/' +  
            '2020_BUZA_ISR_PAL_T/models/Yasin_1999_Eastern_Basin/python/')

if not src_dir in sys.path:
    sys.path.insert(0, src_dir)

from set_paths import proj_dir, model_dir, src_dir, dems_dir, test_dir
from set_paths import tools_dir, exe_dir

if not tools_dir in sys.path:
    sys.path.insert(0, tools_dir)

print(sys.version)
print(sys.executable)

os.chdir(test_dir)

# convenience
AND = np.logical_and
NOT = np.logical_not

from yasin_mf6_model import get_new_roof_of_model

class Test_grid(unittest.TestCase):

    def setUp(self):
        with open(os.path.join(test_dir, 'yasin_old.pickle'), 'rb') as fp:
            yasin_old = pickle.load(fp)

        get_new_roof_of_model(yasin_mf6, src_dir=None, tifname='wbank_6984_km.tif',
                plot=False, out=False)

        pass

    def test_old_grid(self):
        pass

    def tearDown(self):
        pass

gr_surface_tif = os.path.join(dems_dir, 'mdl_elev.tif')


# %% Check layer thickness of the old model
gr_old = yasin_old['bcf']['gr']
ibound = yasin_old['bas']['IBOUND']

inactive = ibound == 0 # inactive cells
DZ = np.ma.masked_array(gr_old.DZ, inactive)
# %%
# Spy active cells 
fig, axs = plt.subplots(1,gr_old.nlay, sharex=True, sharey=True, figsize=(6, 12))
for ilay in range(gr_old.nlay):
    axs[ilay].set_title('Layer {}'.format(ilay))
    axs[ilay].grid()
    axs[ilay].spy(NOT(inactive[ilay])) # , DZ[ilay] <= 100 * gr_old.min_dz))

# Show the min thickness of each layer
for ilay in range(gr_old.nlay):
    print("Min thickness of layer {}, active {:6.2f} m, inactive {:6.2f} m".format(
        ilay,
        np.min(gr_old.DZ[ilay][NOT(inactive[ilay])]),
        np.min(gr_old.DZ[ilay][inactive[ilay]]))
        )

# %% # %% Check layer thickness of the new model
with open(os.path.join(test_dir, 'yasin_mf6.pickle'), 'rb') as fp:
    yasin_mf6 = pickle.load(fp)

gr_mf6 = yasin_mf6['dis']['gr']
idomain = yasin_mf6['dis']['idomain']

inact_mf6 = idomain == 0 # inactive cells
DZ_mf6 = np.ma.masked_array(gr_mf6.DZ, inact_mf6)

# %%
# Spy active cells 
fig, axs = plt.subplots(1,gr_mf6.nlay, sharex=True, sharey=True, figsize=(6, 12))
for ilay in range(gr_mf6.nlay):
    axs[ilay].set_title('Layer mf6{}'.format(ilay))
    axs[ilay].grid()
    axs[ilay].spy(NOT(inact_mf6[ilay])) # , DZ[ilay] <= 100 * gr_old.min_dz))

# Show the min thickness of each layer
for ilay in range(gr_mf6.nlay):
    print("Min thickness of layer {}, active {:6.2f} m, inactive {:6.2f} m".format(
        ilay,
        np.min(gr_mf6.DZ[ilay][NOT(inact_mf6[ilay])]),
        np.min(gr_mf6.DZ[ilay][inact_mf6[ilay]]))
        )


#%% spy if / where ibound != 0 differs from idomain !=0
fig, axs = plt.subplots(1,gr_mf6.nlay, sharex=True, sharey=True, figsize=(6, 12))
for ilay in range(gr_mf6.nlay):
    axs[ilay].set_title('Layer mf6{}'.format(ilay))
    axs[ilay].grid()
    axs[ilay].spy(AND(ibound[ilay]!=0, idomain[ilay]!=0)) # , DZ[ilay] <= 100 * gr_old.min_dz))

# Assertion Test ok.
print(np.any(AND(idomain == 0, ibound != 0)))
print(np.any(AND(idomain != 0, ibound == 0)))

# %%

# TODO What has been done with the old grid to get a new one with
# different layer thicknesses?