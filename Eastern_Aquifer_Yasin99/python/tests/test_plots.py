
import os, sys, pickle
import matplotlib.pyplot as plt


src_dir = '/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/Yasin_1999_Eastern_Basin/python/'

os.chdir(src_dir)
if not src_dir in sys.path:
    sys.path.append(src_dir)

from set_paths import proj_dir, model_dir, src_dir, dems_dir, test_dir
from set_paths import tools_dir

if not tools_dir in sys.path:
    sys.path.insert(0, tools_dir)

import yasin_mf6_model as ymf6
from etc import newfig

os.chdir(test_dir)

ax = newfig("Plot boundaries", "x [m]", "y [m]", aspect=1)
if True:
    with open(os.path.join(test_dir, 'yasin_mf6.pickle'), 'rb') as fp:
        yasin_mf6 = pickle.load(fp)
        ymf6.plot_bounds(yasin_mf6, blist=None, layer=0, type='wel', ax=None)
else:
    ymf6.plot_bounds(ymf6.yasin_mf6, blist=None, layer=0, type='wel', ax=None)

os.chdir(src_dir)