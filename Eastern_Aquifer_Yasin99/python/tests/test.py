#!/
# usr/bin/env python
# -*- coding: utf-8 -*-

# %%
import sys
print("sys imported.")

# %%
print("\nsys.path line by line:")
print('\n')
for p in sys.path:
    print(p)
print("Done !\n")
# %%
