# %%

import os, sys
import numpy as np
import matplotlib .pyplot as plt
import pyproj
import rasterio


# %%
crs = pyproj.CRS.from_epsg(4326)
crs = pyproj.CRS.from_string("epsg:4326")
crs = pyproj.CRS.from_proj4("+proj=latlon")
crs = pyproj.CRS.from_user_input(4326)
# %%
crsTim = pyproj.CRS.from_epsg(32636)
crsISR = pyproj.CRS.from_epsg(6984)
wgs84 = pyproj.CRS.from_epsg(4326)
# %%

fname = '../GWB1_Groundwater_Recharge/monthly/GWB1_qdown_Groundwater_Recharge_mm-month_monthly_2003.01.01.tif'
with rasterio.open(fname) as src:
    print(src.profile)
# %%

