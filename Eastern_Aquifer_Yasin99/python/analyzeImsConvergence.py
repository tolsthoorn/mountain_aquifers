# Analysis of the ongonig convregence using the IMS module of mf6.
"""
By specifying the ims_inner and ims_outer file names, the running convergence
for the inner and outer iterations is recorded in the two csv files. This
convergence can then be subsequently anayzed.

This is done by importing them into pandas DataFrames followed by desired
presentation or analysis performed on them.

Important information is how many inner iterations were required until convergence
is reached and the next outer iteration is started. Also it is analyzed at which
cells the largest residual occurs at the beginning or end of each iteration
cycle, which may point to the possible error, such as wells pumping in a dry
aquifer or cells surrounded by only inactive cells.
"""

# %%
import pandas as pd
import os
import sys
import numpy as np
import mkpath
from etc import attr, newfig, color_cycler
import logging
import pandas as pd
from fdm import Grid

x = np.arange(200000, 248000,  1000)
y = np.arange(683000, 584000, -1000)
z = np.arange(0, -5, -1)
gr = Grid(x, y, z)

logging.disable(sys.maxsize) # switch off logging of imported modules

ws = '/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/Yasin_1999_Eastern_Basin/python/'
data = os.path.join(ws, 'yasin_mf6')
os.chdir(ws)

ims_outer = os.path.join(data, 'ims_outer.csv')
ims_inner = os.path.join(data, 'ims_inner.csv')

#%%
def nodes2lrc(nodes, shape, zero_based=True):
    """Return LRC list of tuples given nodes and shape of regular grid.
    
    nodes: array of ints
        the node numbers
    shape: 3-tuple
        the shape of the model (layer, row, col)
    """
    _, nrow, ncol = shape

    # The node numbers are 1-based, so treat them as such.
    # Convert to zero-based lrc afterwards if so desired
    ilay = np.asarray(np.floor(nodes / (nrow * ncol)), dtype=int) + 1
    irest = nodes - (ilay - 1) * nrow * ncol
    irow = np.asarray(np.floor(irest / ncol), dtype=int) + 1
    icol = irest - (irow - 1) * ncol
    if zero_based:
        return [(il - 1, ir - 1, ic - 1) for il, ir, ic in zip(ilay, irow, icol)]
    else:
        return [(il, ir, ic) for il, ir, ic in zip(ilay, irow, icol)]

def get_ims_csv_data(ims_inner, ims_outer, shape):

    ii = pd.read_csv(ims_inner)
    io = pd.read_csv(ims_outer)

    idvnd = ii['solution_inner_dvmax_node'].values
    idrnd = ii['solution_inner_drmax_node'].values
    




# %% # Find where data start for inner and outer and the number of rows to read for each of them

def get_header_ln(mfsim_output_fname):
    "Return header lines and data lines"

    with open(mfsim_output_fname, 'r') as fp:
        n_line = -1
        n_outer = 0
        n_inner = 0
        hdr1 = [0, 0, 0]
        hdr2 = [0, 0, 0]
        for line in fp:
            n_line += 1
            if   line.startswith(' OUTER ITERATION SUMMARY'):
                hdr1 = [n_line + 2, n_line + 3, n_line + 5]
            elif line.startswith(' INNER ITERATION SUMMARY'):
                hdr2 = [n_line + 2, n_line + 3, n_line + 5]
            elif line.startswith(' Model  '):
                n_outer += 1
            elif hdr2[0] and line.startswith('      ') and line.endswith(')\n'): #  :
                n_inner += 1
            else:
                pass
        print('Loop ended when n_line = {:5d}'.format(n_line))
    return hdr1, hdr2, n_inner, n_outer, n_line

# %% Read the outer and inner iteration data into separate data_frames

def get_tables(mfsim_output_fname):
    """Return the outer and inner iteration tables as pandas DataFrames.
    
    These are the tables constructed in the .cvs files in which the IMS module
    stored the ongoing outer and inner interation symmaries. The file names are
    specified in the kwargs of the IMS module in flopy mf6.
    
    """
    
    hdr1, hdr2, n_inner, n_outer, n_line = get_header_ln(mfsim_output_fname)
    print('Outerheader={}, n_outer={}'.format(hdr1, n_outer))
    print('Innerheader={}, n_inner={}'.format(hdr2, n_inner))

    names_outer = ['Model', 'i_outer', 'i_inner', 'maxdh', 'cellid_dh']
    names_inner = ['i_tot', 'i_outer', 'i_inner', 'maxdh', 'cellid_dh', 'maxdq', 'cellid_dq']

    tbl_outer = pd.read_table(mfsim_output_fname, header=None, sep=' ',
        engine='python', skipinitialspace=True, skiprows=hdr1[-1], nrows=n_outer, skip_blank_lines=False,names=names_outer)

    tbl_inner = pd.read_table(mfsim_output_fname, header=None, sep=' ',
        engine='python', skipinitialspace=True, skiprows=hdr2[-1], nrows=n_inner, skip_blank_lines=False,names=names_inner)
    return tbl_outer, tbl_inner

#%% Show the outer iteration data table
def iter_progress():
    pd_outer = pd.read_csv(ims_outer, header=0)
    pd_inner = pd.read_csv(ims_inner, header=0)
    
    ax = newfig("Iteration progress", "nouter", "inner_iterations")
    ax.plot(pd_outer['nouter'], pd_outer['inner_iterations'], label='inner_iterations')
    ax.legend()

    ax = newfig("Iteration progress", "nouter", "abs(dv_max)", yscale='log')
    ax.plot(pd_outer['nouter'], pd_outer['solution_outer_dvmax'].abs(), label='outer_dvmax')
    ax.legend()
    return pd_outer, pd_inner

def main(mfsim_output_fname):
    """Get iteration data and show plots."""
    
    tbl_outer, tbl_inner = get_tables(mfsim_output_fname)

    print('Outer iteration table:')
    print(tbl_outer)
    print('\n\n\n\n')
    print("Inner iteration table:")
    print(tbl_inner)

    ax = newfig("Outer iterations", "i_outer", "log dv", yscale='log')
    ax.plot(tbl_outer.index, tbl_outer['maxdh'].abs())
    ax.legend()

    ax1 = newfig("Inner iterations", "i_tot", "log dv", yscale='log')
    ax1.plot(tbl_inner.index, tbl_inner['maxdh'].abs(), label='maxdh')
    ax1.plot(tbl_inner.index, tbl_inner['maxdq'].abs(), label='maxdq')
    ax1.legend()
    return None

# %%
if __name__ == '__main__':

    ii = pd.read_csv(ims_inner)
    io = pd.read_csv(ims_outer)

    nodes = ii['solution_inner_dvmax_node'].values
    shape = 4, 98, 47
    lrc0 = nodes2lrc(nodes, shape, zero_based=True)

    ax = newfig("Solution_inner_dvmax", "total_inner_iterations", "solution_inner_dvmax", yscale='log')
    ax.plot(ii['total_inner_iterations'], ii['solution_inner_dvmax'],
     label='dvmax', lw=0.25)
    ax.plot(ii['total_inner_iterations'], ii['solution_inner_drmax'],
     label='drmax', lw=0.25)

    ax = newfig("Solution_inner_dvmax_node", "total_inner_iterations", "solution_inner_dvmax", yscale='linear')
    ax.plot(ii['total_inner_iterations'], ii['solution_inner_dvmax_node'],
     label='dvmax', lw=0.25)
    ax.plot(ii['total_inner_iterations'], ii['solution_inner_drmax_node'],
     label='drmax', lw=0.25)

    lrc_dv = np.asarray(nodes2lrc(ii['solution_inner_dvmax_node'].values,
     shape=shape, zero_based=True), dtype=int).T
    lrc_dr = np.asarray(nodes2lrc(ii['solution_inner_drmax_node'].values,
     shape=shape, zero_based=True), dtype=int).T

    clrs = color_cycler()

    axdv = newfig("dvmax_node", "x [m]", "y [m]")
    axdr = newfig("drmax_node", "x [m]", "y [m]")

    for ilay in range(shape[0]):
        clr = next(clrs)
        axdv.plot(gr.xm[lrc_dv[2][lrc_dv[0]==ilay]], gr.ym[lrc_dv[1][lrc_dv[0]==ilay]],
         color=clr,  ls='none', marker='.', label='lrc_dv' + '_' + clr)
        axdr.plot(gr.xm[lrc_dr[2][lrc_dr[0]==ilay]], gr.ym[lrc_dr[1][lrc_dr[0]==ilay]],
         color=clr, ls='none', marker='.', label='lrd_dr' + '_' + clr)
    axdv.legend()
    axdr.legend()


    print("Done.")
    #tbl_outer, tbl_inner = get_tables(ims_inner, ims_outer)

    #iter_progress()
# %%
