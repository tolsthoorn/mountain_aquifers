import os
import sys
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
from matplotlib import patches
import shapefile
import logging
logging.basicConfig(level=logging.WARNING, format=' %(asctime)s - %(levelname)s - %(message)s')
#logging.disable(logging.DEBUG)


from setPaths import folders

shps_dict = {'Lake Kinnereth.shp': {'ec':'darkblue', 'fc':'darkblue'},
    'Dead Sea total.shp': {'ec':'darkblue', 'fc':'darkblue'},
    'Dead Sea 3 Jor.shp': {'ec':'darkblue', 'fc':'darkblue'},
    'Dead Sea 2 isr.shp': {'ec':'darkblue', 'fc':'darkblue'},
    'Contours.shp': {'c': 'gray', 'lw':0.25},
    'Jordan.shp': {'c': 'blue', 'lw':1},
    'Green Line border.shp': {'c': 'green', 'lw':2},
    'Mountain peaks.shp': {'marker': '.', 'mfc': 'black', 'color': 'black'},
    'Places.shp': {'marker': 'o', 'mfc': 'orange', 'color': 'orange'},
    }

# %% Plotting all shape files
def plot_objects(gis_dir:None, shps_dict:None, ax=None):
    """Plot all object on given axes.
    
    Parameters
    ----------
    gis_dir: str
        name of path to gis directory with the shapefiles.
    shps_dict: dict
        keys are the basename fo the shapefiles
        values are a kwargs dict.
    ax: matplotlib.pyplot.Axes object
        the axis to plot onto.
    """
    for basename in shps_dict:
        fpath = os.path.join(gis_dir, basename)
        plot_shape(fpath, ax=ax, **shps_dict[basename])

# %% Plotting a single shapefile
def plot_shape(fpath, ax=None, **kwargs):
    """Plot contents of a shapefile.
    
    Parameters
    ----------
    fpath: str
        full path to shapefile
    ax: matplotlib.pyplot.Axes object
        axes to plot on
    kwargs: dict
        parameters to pass on to underlying plot functions.
    """
    with shapefile.Reader(fpath) as sf:
        if sf.shapeTypeName.startswith('POLYLINE'): # Line shapes
            for sh in sf.shapes():
                points = np.array(sh.points).T
                ax.plot(*points, label=os.path.basename(fpath), **kwargs)
        if sf.shapeTypeName.startswith('POINT'):
            for sh, rec in zip(sf.shapes(), sf.records()):
                fsz   = kwargs.pop('fs', 6)
                color = kwargs.pop('color', 'orange')
                ax.plot(*sh.points[0], **kwargs)
                ax.text(*sh.points[0], rec[1], color=color, fontsize=fsz)
        if sf.shapeTypeName.startswith('POLYGON'):
            for sh, rec in zip(sf.shapes(), sf.records()):
                vertices = sh.points
                codes = np.ones(len(vertices), dtype=int)
                codes[0]  = patches.Path.MOVETO
                codes[1:] = patches.Path.LINETO
                codes[-1] = patches.Path.CLOSEPOLY
                path = patches.Path(vertices=vertices,
                                codes=codes, closed=True)
                patch = patches.PathPatch(path, **kwargs)
                ax.add_patch(patch)

if __name__ == '__main__':
    logging.warning("Appending 4 folders to sys.path.")
    for key in ['src_python', 'tools', 'gis', 'test']:
        if not folders[key] in sys.path:
            sys.path.append(folders[key])

    from etc import newfig
    
    os.chdir(folders['test'])
    logging.warning("cwd = {}".format(os.getcwd()))


    ax = newfig("Shapefile features, Isr-Pal Mountain Aquifer",
                    'x [m EPGS_6489]', 'y [m EPGS_6484', figsize=(8, 8))

    plot_objects(gis_dir=folders['gis'], ax=ax, shps_dict=shps_dict)

    plt.show()

#%%
