#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 15 16:51:50 2021

First Flopy mdf6 tutorial from the flopy documentation

This is the same probleme as tutorial01, with but the chd with the model
interior is replaced by a well with a time series as extraction input and also
the recharge has been defined as a timearrayseries.Further the adding of packages
to the model has been separated from the specificiation of the input data.
This implies that the adding of packages now is essentially independent of
the specificiation of the input data. More packages may have to be added
in a similar manner.

See for the examples on how to use the ts (time series) and tas (time array series)

https://github.com/modflowpy/flopy/blob/develop/examples/Notebooks/flopy3_mf6_obs_ts_tas.ipynb

To see how stress period data can be specified see
https://flopy.readthedocs.io/en/3.3.4/_notebooks/tutorial06_mf6_data.html
Note that the cellid information (layer, row, column) is encapsulated in a tuple.


# The coordinate system is EPGS:2039, that is, it was the old palestine system.
The EPGS:2039 E = Palestine E +  50 km
The EPGS:2039 N = Palestine N + 500 km

The limits of the area of interest will be E   N

31.3N - 32.6N # Decimal degrees
34.6E - 35.6E # Decimal degrees


@author: Theo 2021-22-16
"""
# %%
#import inspect
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
import pandas as pd
import pickle
import warnings
import rasterio
import flopy
import logging
logging.basicConfig(level=logging.WARNING, format=' %(asctime)s - %(levelname)s - %(message)s')

os.chdir(
    os.path.join('/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/',
             '2020_BUZA_ISR_PAL_T/mountain_aquifers/Eastern_Aquifer_Yasin99/python/')
)
logging.warning("cwd = {}".format(os.getcwd()))

from setPaths import folders
from mountainAquiferShapes import shps_dict, plot_objects
from etc import newfig, color_cycler, line_cycler # tools.etc.newfig
from fdm.mfgrid import Grid # tools.fdm.mfgrid

def whoAmI():
    """Return name of current file."""
    return sys._getframe(2).f_code.co_name

def whoIsCalling():
    """Print which function name from within the function."""
    str = "\nFunction " + whoAmI()
    print(str)
    print(len(str) * '=')

logging.warning(sys.version)
logging.warning(sys.executable)


# %%
# src_dir is the directory with the python source files defined in setpaths
# It is the startup diretory, otherwise it can't work.
os.chdir(folders['src_python'])
logging.warning("cwd = {}".format(os.getcwd()))

gr_surface_tif = os.path.join(folders['dems'], 'mdl_elev.tif')

with open(os.path.join(folders['test'], 'yasin_old.pickle'), 'rb') as fp:
    logging.warning("Retrieving `yasin_old.pickle` from folders[{}]".format('test'))
    yasin_old = pickle.load(fp)

# %% Get the mf_6 model data from the old model

def get_new_mf6_model(yasin_old, tif_path=None, chd_dead_sea_coast=None, drains_present=False, strategy=3):
    """Return input for the new mf6 model by refatoring the old mf96 model.

    We'll use the same model as yasin_old, except that we'll use a fully
    3D model instead of a quasi 3D model. The number of layers will be the same,
    therefore, most of the inputs are actually the same for the new and the old
    model. The only exception are the vertical conductivities. The original model
    didn't have them because it used a quasi-3D approach with VCONT between the
    aquifers. However, the diretory with the original files also had a file 'trial2.lea' which, according to Chiang & Kinzelbach (2003) is a file with vertical
    conductivities. I checked that these vertical conductivities were compatible
    with the VCONT of the quasi-3D model. Hence they can be used instead of the
    VCONT for the new fully 3D model. This makes sure the vertical conductivities
    of the new model's layers are uniquely defined, which is impossible if only
    the VCONT is given (which combines both the vertical conductivties and the
    thicknesses of the two overlying aquifers for which the VCONT should hold).

    Parameters
    ----------
    yasin_old: dict
        the data extraced from the original Yasin 1999 data files.
    tif_path: str
        Full path to the tif hold in the ground surface elevation. The tif must contain the area
        area of interest given by gr.x and gr.y and be in new Israel-Palestine coordinates.
    chd_dead_sea_coast: float or None
        The level of the Dead Sea to be used. If None, no direct connection to the
        Dead Sea is used, exactly as it was in the original 1999 Yasin model.
        Dead Sea Levels:
            None   : don't use Dead Sea
            -380   : (1950)
            -411   : (1999)
            -430.5 : (Recent, Wikipedia)
        Set hDeadSea to one of the concrete values to use the Dead Sea as boundary.
    drains_present: bool
        Whether drains on ground surface are required or not.
    strategy: int, oneof 1, 2, 3
        Boundary-head correction strategy to correct for head elevation not within specified layer in old model
        1): raise h to above the bottom of the cell of the current, specified layer.
            This assumes cell was right, but specified h was wrong.
        2): remove the record altogether (assuming situation is impossible)
        3): transfer the boundary condition to highst cell with bottom below h.
            This assumes that the h was right, but cell layer was wrong.
    """
    newmodel = 'yasin_mf6'
    logging.warning("Constructing the new model using function `{}`".format(whoIsCalling()))
    # The original model data
    gr_old = yasin_old['bcf']['gr']

    ibound = yasin_old['bas']['IBOUND']

    logging.warning("Using dem as new roof on `{}'.".format(newmodel))
    gr = get_new_roof_of_model(gr_old, tif_path=tif_path, plot=False, ibound=ibound)    
 
    # Initialize the new model arrays
    K1     = yasin_old['bcf']['HY']
    K3     = yasin_old['bcf']['LEA']
    strt   = yasin_old['bas']['STRT']
    
    # This still has the old elevation for the top of the model (data=Z)

    # %% Using mf6 idomain
    idomain = np.ones_like(ibound, dtype=int) # start with all cells active
    idomain[ibound == 0] = 0 # only ibound == 0 are inactive cells in mf6
    idomain[gr.DZ <= gr.min_dz] = -1  # Inactive, vertically flow-through cell
    #idomain[np.logical_and(idomain != 0, gr.DZ <= gr.min_dz)] = -1  # Inactive, vertically flow-through cell
    # Special: Don't allow inactive cells in layer 3 while layer 2 is active
    idomain[-1][np.logical_and(idomain[-2] > 0, idomain[-1] <= 0)] = 1

    yasin_mf6 = dict()
    bas = yasin_old['bas']

    yasin_mf6['formations'] = {
        0: {'name': 'Sononian', 'color': 'magenta'},
        1: {'name': 'Upper Cenomanien', 'color': 'yellow'},
        2: {'name': 'Yatta', 'color': 'darkblue'},
        3: {'name': 'Lower Cenomanien', 'color': 'green'}
        }

    yasin_mf6['tdis'] = {'NPER': bas['NPER'], 'NSTP': bas['NSTP'], 'TSMULT': bas['TSMULT']}

    NPER = yasin_mf6['tdis']['NPER']

    yasin_mf6['sto']  = {'storagecoefficient': False,
                         'iconvert':  idomain >0, # exclude -1 (flow-through, inactive) and 0  (inactive)
                         'ss': 1e-5,  # should be a 3D array
                         'sy': 0.15,  # should be a 3D array
                         'steady_state': np.ones( NPER, dtype=bool),
                         'transient':    np.zeros(NPER, dtype=bool),
                        }
    yasin_mf6['dis'] = {'length_units': 'meters',
                        'gr': gr,
                        'proj': yasin_old['bcf']['proj'],
                        'idomain': idomain,
                        'ibound': ibound, # comes in handy, don't stricktly need it.
                        }
    yasin_mf6['npf'] = {'k1': K1,
                        'k3': K3,
                        'perched': True,
                        'icelltype': 1,
                        }
    yasin_mf6['ic'] = {'strt': strt}
    
    yasin_mf6['boundHeadCorrStrategy'] = strategy # 1, 2, 3 

    well_spd = put_well_in_proper_layer(yasin_old['wel'], gr=gr)
    ghb_spd = raise_hd_above_bottom(yasin_old['ghb'], gr=gr, idomain=idomain, strategy=strategy)
  
    # CHD
    chd_spd = yasin_old['chd']
    dead_sea_present =  chd_dead_sea_coast is not None   
    if  dead_sea_present:
        chd_spd = combine_chd_spd_with_dead_sea(chd_spd, chd_dead_sea_coast, idomain=idomain)
    chd_spd = raise_hd_above_bottom(chd_spd, gr=gr, idomain=idomain, strategy=strategy)

    yasin_mf6['rch'] = {'stress_period_data': yasin_old['rch']['RECH'],
                        'irch':get_irch(idomain, zero_based=True)}
    yasin_mf6['ghb'] = {'stress_period_data': ghb_spd}
    yasin_mf6['wel'] = {'stress_period_data': well_spd}
    yasin_mf6['chd'] = {'stress_period_data': chd_spd,
                        'chd_dead_sea_coast': chd_dead_sea_coast,
                        'dead_sea_present' : dead_sea_present,
                        }

    # Surface runoff conductance per m, hard_wired
    yasin_mf6['drn'] = {'present': drains_present, 'conductance' : 10.}

    if drains_present:
        drn_spd = get_drains(gr=gr, idomain=idomain)
        yasin_mf6['drn'] = {'stress_period_data': drn_spd,
                            'conductance': 10,
                           }

    # side-effect: pickle the new model
    with open(os.path.join(folders['test'], 'yasin_mf6.pickle'), 'wb') as fp:
        logging.warning("Dumping (pickle) the new model `{}` in folders[{}]".format(newmodel, 'test'))
        pickle.dump(yasin_mf6, fp)
    logging.warning("returning `{}`".format(newmodel))
    return yasin_mf6

# %% Define how to plot hydraulic boundary cells

def plot_bounds(yasin_mf6, iper=-1, layer=0, type='wel', ax=None, **kwargs):
    """Plot the boundaries of given type on map with ax.
    
    Parameters
    ----------
    yasin_mf6: dict with the data
        The data that construct the mf6 model.
    blist: list of boundary cells [(j, j, i), h, ...), (...), ...]
        the cells to be plotted
    layer: int
        zero-base model layer number
    type: str
        boundary package name
    ax: plt.axes
        axis on which to plot
    kwargs dictionary
        passed on to ax.plot(..)
    """
    from etc import plot_kw, text_kw

    plt_kw = set(kwargs.keys()).intersection(plot_kw.keys())
    txt_kw = set(kwargs.keys()).intersection(text_kw.keys())
    pkwargs = {k:kwargs[k] for k in plt_kw}
    tkwargs = {k:kwargs[k] for k in txt_kw}

    gr = yasin_mf6['dis']['gr']

    dtypes = {
        'chd': np.dtype([('cellid', 'O'), ('head', '<f8')]),
        'wel': np.dtype([('cellid', 'O'), ('q', '<f8')]),
        'drn': np.dtype([('cellid', 'O'), ('elev', '<f8'),
                                ('cond', '<f8')]),
        'ghb': np.dtype([('cellid', 'O'), ('bhead', '<f8'),
                                ('cond', '<f8')]),
        'riv': np.dtype([('cellid', 'O'), ('stage', '<f8'),
                        ('cond', '<f8'), ('zbot', '<f8')])}
        
    assert type in dtypes, "type {} not in [{}]".format(dtypes.keys())

    try:
        # bounds is converted to a recarray of given dtype
        blist = yasin_mf6[type]['stress_period_data'][iper]
    except:
        warnings.warn("Model may not have boundaries of type {} "
                .format(type))
        return
    try:
        bounds = np.array(blist, dtype=dtypes[type])
    except:
        raise ValueError('Boundary cell list may not be ' +
                'compatible with the dtype for the given package.')

    # Always print the head, flow or elevation, the name of which is
    pname = bounds.dtype.names[1] # parameter name from recarray fields

    for cellid, pvalue in zip(bounds['cellid'], bounds[pname]):
        k, j, i = cellid        
        if k  == layer:
            ax.plot(gr.xm[i], gr.ym[j], **pkwargs)
            ax.text(gr.xm[i], gr.ym[j],
                '_{}={:.0f}'.format(type, pvalue), **tkwargs)
    return bounds


def get_irch(idomain, zero_based=True):
    """Return an array (ny, nx), i.e. irch, with the layer number of the top active cells.
    
    Also useful to place drains on ground surface (in first active vertical cell).

    Irch using in RCH package is the array telling in which layer the recharge will work. This is the top most layer in a column that is active.

    According to flopy.mfrch source code docstring, layer must be zero-based that
    implies top model layer has zero as layer number when the cell in the top layer is
    inactive or when it is the first active cell. This, however is tricky to implement.
    The easiest way is to assume irch is one-based, then the top layer having zero
    means inactive. Afterwards, we can subtract 1 from all cells with value > 0.

    If you want the top layer which is active just use zero_based = False.
    The the array will have 0 for inactive, 1 for cells that are active in the
    top layer, 2 for cells that are active in the second layer but not in the first,
    3 for cells that are active in the 3rd layer but not in the first and second, etc.
    This can be used to determine the cells that are actually at ground surface, which
    is what irch does express.

    Parameters
    ----------
    idomain : ndarray (nlay, nrow, ncol)
        mf6 array telling wheather a cell is active [>0] or inactive [0] or
        a vertical flow-through cell [-1].
    zero_based : bool
        if True first layer has index 0 (flopy), else 1 (modflow)

    @TO 20210630
    """
    irch = np.zeros_like(idomain[0])
    for iz in range(len(idomain)):     
        laynr = iz + 1
        irch[np.logical_and(irch == 0, idomain[iz] > 0)] = laynr
    if zero_based:
        irch[irch > 0] -= 1
    return irch


def put_well_in_proper_layer(well_data, gr=None):
    """Make sure the well is in a sufficient thick active layer.s

    Parameters
    ----------
    well_data: dict
        stress-period indexed stress period data for well extraction
    gr: tools.fdm.Grid object
        Structured Modflow Grid
    """
    for iper in well_data.keys():
        new_list = [] # rectified boundary condion list
        for rec in well_data[iper]: # rec must be (k, j, i, h, ...)
            # Put the well in a thicker lower layerse if it exists
            (k, j, i), q = rec # note k, j, i are zero based
            while k < gr.nz and gr.DZ[k, j, i] <= gr.min_dz:
                k += 1
                rec = (k, j, i), q
            new_list.append(rec) # Store this rec (always succeeds)
        well_data[iper] = new_list

    return well_data


def raise_hd_above_bottom(sp_data, gr=None, dz=0.1,
                                idomain=None, strategy=3):
    """Raise head/elev in stress period data to at least dz above layer bottom, in place.
    
    Essential to ensure boundary pakage heads are valid to convertible modflow6 cells.

    The idea is that the j, j, i are correct, but then we cannot have a head below
    the bottom of the layer for which the condition is defined.
    It is not clear which of these strategies is correct.

    Parameters
    ----------
    sp_data: dict with keys sp number and list of cell records [(k, j, i, h, ...), ...]
        Stress period data.
    stratagy: oneof 1, 2, 3
        1): raise h to above the bottom of the cell of the current, specified layer.
            This assumes cell was right, but specified h was wrong.
        2): remove the record altogether (assuming situation is impossible)
        3): transfer the boundary condition to highst cell with bottom below h.
            This assumes that the h was right, but cell layer was wrong.
    gr: fdm.Grid object
        The modflow Grid (block shape, regular)
    idomain: int array of size gr.shape
        tells wether a cell is active (>0) inactive (<0) and or
        flow=through (-1) which is also inactive
    dz: float > 0
        The minimum elevation of head above bottom of current layer.
    """
    active = idomain > 0
    for iper in sp_data.keys():
        new_list = [] # rectified boundary condion list
        for rec in sp_data[iper]: # rec must be (k, j, i, h, ...)
            (k, j, i), h, *rest = rec # note k, j, i are zero based
            if strategy == 1: # Raise head or elev to dz above bottom of layer
                # rest[0] is the first value of rest, i.e. the head (or flow if wel)
                # Adapt the head, it must be above the bottom of the given cell.
                h = max(h, gr.Zbot[k, j, i] + dz)
                # Generate a new record with adapted head
                rec = (k, j, i), h, *rest
                new_list.append(rec) # Store this rec (always succeeds)
            elif strategy == 2: # Remove record
                if h >= gr.Zbot[k, j, i]:
                    new_list.append(rec) # Success, store this rec
                else:
                    pass # No success, ignore this rec
            elif strategy == 3:
                # Change k to that of the cell with bottom below h,
                # which must also be active
                # This assumes that h < Zbot(nlay, j, i) !!
                for kk in range(k, len(gr.Zbot)):
                    if np.logical_and(
                        gr.Zbot[kk, j, i] < h, active[kk, j, i]):
                        rec = (kk, j, i), h, *rest
                        new_list.append(rec) # Store this rec
                        break # Found, ready
                    else:
                        pass # try next deeper cell,
                             # if deepest, ignore rec
            else:
                raise ValueError("strategy must be oneof [1, 2, 3].")
        sp_data[iper] = new_list

    return sp_data

# %% Simulation

def get_modflow_grid(gr_old, gr_surface_tif=None): # Obsolete
    """Get the modflow grid for the yasin_mf6 model

    The old grid see yasin_old['bcf']['gr'] already has the x and y coordinates in EPGS6984 projection. However, its top layer elevation is still arbitrary;
    it was set aribtrrily to 100 m above the base of the  first layer.
    Therefore, the true ground elevation has to be used as the roof of the
    modflow grid, i.e. as top of the first layer.

    The elevation (dem) data are in ../mdl_elev.tif

    Parameters
    ----------
    gr_old: tools.fdm.Grid object
        Blockshaped modflow grid
    """
    # Check layer thickness
    def spy_layer_thickness(gr_old, gr_new):
        """Shee where ground surface is lower than the top of the second layer (bottom of layer 0)"""
        DZold = gr_old.DZ
        DZnew = gr_new.DZ

        inactive = yasin_old['bas']['IBOUND'] == 0

        dz = (gr_new.Z[0][np.newaxis, :, :] - gr_old.Z[1:]).reshape(gr_new.nlay * gr_new.nrow, gr_new.ncol)
        dv = 10
        vmin, vmax = np.floor(dz.min()  / dv) * dv, np.ceil(dz.max() / dv) * dv

        f0 = DZold.reshape(gr_old.nlay * gr_old.nrow, gr_old.ncol) >= gr_old.min_dz
        f1 = DZnew.reshape(gr_new.nlay * gr_new.nrow, gr_new.ncol) >= gr_new.min_dz
        f0 = dz > 0
        f1 = dz < 0

        fig, axs = plt.subplots(1, 3)
        fig.set_size_inches(12, 10)

        for ax,title, xlabel, ylabel, what in zip(axs, ['dz > 0', 'dz < 0', 'dz'], ['ixm', 'ixm', 'x[m]'], ['iym', 'iym', 'z'], [f0, f1, None]):
            ax.grid()
            ax.set_title(title)
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)
            ax.set_aspect(1.0)
            if what is not None:
                ax.spy(what)
            else:
                #c = ax.imshow(dz, extent=gr_new.extent, vmin=vmin, vmax=vmax, cmap='viridis')
                #c = ax.imshow(dz, extent=gr_new.extent, vmin=-500, vmax=500, cmap='viridis')
                c = ax.imshow(dz, extent=gr_new.extent, vmin=-500, vmax=500, cmap='Accent')
                #c = ax.contourf(dz, levels=np.linspace(-500, 500, 21), cmap='viridis')
                #ax.contour(dz, levels=np.linspace(-500, 500, 21), colors='k', linewidths=0.25)
                ax.set_aspect(4.)
                plt.colorbar(c)

        fig, axs = plt.subplots(1, 7, sharex=True, sharey=True)
        fig.set_size_inches(12, 4)
        rows = [20, 30 , 40, 50, 60, 60, 80]

        Ztop = gr_new.Ztop
        Zbot = np.ma.masked_array(gr_old.Zbot, mask=inactive)

        for ax,row in zip(axs, rows): 
            ax.grid()
            ax.set_title('row {}'.format(row))
            ax.set_xlabel('x[m]')
            ax.set_ylabel('y[m')
            ax.plot(gr_new.xm, Ztop[0, row], 'b-', label='top0')
            ax.plot(gr_new.xm, Zbot[1, row], 'r-', label='bot1')
            ax.plot(gr_new.xm, Zbot[2, row], 'g-', label='bot2')
            ax.plot(gr_new.xm, Zbot[3, row], 'k-', label='bot3')
            ax.legend()

        fig, axs = plt.subplots(1, 4, sharex=True, sharey=True)
        fig.set_size_inches(12, 4)
        cols = [10, 20, 30 , 40]

        for ax,col in zip(axs, cols): 
            ax.grid()
            ax.set_title('col {}'.format(col))
            ax.set_xlabel('x[m]')
            ax.set_ylabel('y[m')
            ax.plot(gr_new.ym, Ztop[0, :, col], 'b-', label='top0')
            ax.plot(gr_new.ym, Zbot[1, :, col], 'r-', label='bot1')
            ax.plot(gr_new.ym, Zbot[2, :, col], 'g-', label='bot2')
            ax.plot(gr_new.ym, Zbot[3, :, col], 'k-', label='bot3')
            ax.legend()

        plt.show(block=True)

        return None

    # New: Get the elevations from the tif file mdl_elec.tif
    with rasterio.open(gr_surface_tif) as elev_data:
        ztop = elev_data.read(1)
        xm = [(elev_data.transform * (i, 0))[0] for i in range(elev_data.width)]
        ym = [(elev_data.transform * (0, i))[1] for i in range(elev_data.height)]
        assert np.all(gr_old.shape[1:] == ztop.shape), "gr_old.shape = ({},{}) !=\
            ztop. shape ({},{})".format(*gr_old.shape, *ztop.shape
        )
        assert np.all(np.isclose(gr_old.xm, xm)), "gr_old.x not close to x"
        assert np.all(np.isclose(gr_old.ym, ym)), "gr_old.y not close to y"
        Z = gr_old.Z.copy()
        Z[0, :, :] = ztop[:, :]
        gr_new = Grid(gr_old.x, gr_old.y, Z, min_dz = 1.0)
        gr_new.crs = elev_data.crs

    spy_layer_thickness(gr_old, gr_new)

    return gr_new

def set_chd_dead_sea(yasin_mf6, gr=None, idomain=None):
    """Return chd cells where model touches the Dead Sea in deepest model layer.

    Also the Dead Sea cells will be set to active in idomain deepest layer.
    
    Dedicated function for this model and model grid.

    Parameters
    ----------
    yasin_mf6: dict
        The data for the the model as a dict.
    hDeadSea: float
        level of Dead Sea (with respect to datum or MSL)
            None (for no Dead Sea cells)
            -380 # (1950)
            -411 # (1999)
            -430.5 # (Recent, Wikipedia)
    gr: fdm.Grid object
        the model grid
    idomain: ndarray (nz, ny, nx) of int
        tells which cells are active or inactive
        will be updated
    """
    if yasin_mf6['chd']['dead_sea_present'] == False:
        return None

    xDS_west = 220000
    yDS_south, yDS_north = 603000, 630000
    # The range along which the Dead Sea touches the model (y coords)
    dead_sea_touches_model = np.logical_and(gr.ym > yDS_south, gr.ym < yDS_north)

    # To find the right most active cells in the lowest layer, which actually
    # touches the Dea Sea between y = 603000 and 630000, use a copy of domain[-1]
    # and make all its cells west of Xm_west equal to 1 (the right cells are
    # zero becaus inactive. Then sum the rows to get first inactive column number
    # at the y-range where the model touches the Dead Sea.
    ido = idomain[-1].copy() # Only the lowest aquifer touches the Dead Sea
    ido[gr.Xm < xDS_west] = 1
    icol = (np.sum(ido, axis=1) - 1)[dead_sea_touches_model]

    # for rows simply take all rows and select those where dead_sea_touches_model
    irow = np.arange(gr.ny)[dead_sea_touches_model]

    chd_dead_sea_coast = [((gr.nz - 1, irow[i], icol[i]), hDeadSea) for i in range(len(icol))]

    return chd_dead_sea_coast

def get_drains(gr=None, idomain=None):
    """Return drn_spd for drains on ground surface.
    
    Surface runoff is computed using the DRN package, it is the amount
    of water that can't infiltrate and will thus be drained from ground
    surface. We just put drains on top of the model in the top most
    active cells.
    
    In reality, the head stays way below ground surface,
    so DRN is not needed. But, in case the vertical resistance is
    too large, the head will rise far above ground surface.
    """

    # Top active cells
    irch1 = get_irch(idomain, zero_based=False)
    
    # Get the indices of the highest active cells, i.e. of ground surface
    Idr = gr.NOD[0][irch1 > 0] # top layer cells, omitting inactive columns
    lrc = gr.LRC(Idr) 

    # Replace layer number of that of first active cell in vertical column,
    # but now zero based (subtract 1)
    lrc.T[0] = irch1[irch1 > 0] - 1 # return to zero-based

    # Use ground surface as drain elevation (Z[0] covers entire area).
    hdr = gr.Z[0].ravel()[Idr]

    # Use some easily draining conductance for all drains, to capture
    # water that can't infiltrate or seeps out.
    # Notice:  # 1000 m3/d (1 mm/d) for 1 km2 discharges as 1 m head difference.
    Cdr = np.ones(len(Idr)) * yasin_mf6['drn']['conductance']

    # Set up the drains input array for mf6, we use a record array 
    dtype = np.dtype([('cellid', '<i4', 3), ('elev', '<f8'), ('cond', '<f8')])
    drn_spd = np.zeros(len(Idr), dtype=dtype)
    drn_spd['cellid'] = np.asarray(lrc, dtype=[('cellid', 'i4', 3)])
    drn_spd['elev'] = hdr
    drn_spd['cond'] = Cdr

    # Turn into list, because flopy does not accept this perfectly legal record array.
    drn_spd = {0: [(tuple(p[0]), p[1], p[2]) for p in drn_spd]}
    return drn_spd

def combine_chd_spd_with_dead_sea(chd_spd, chd_list, idomain=None):
    """Combine stress period data dict with a list of sp records.
    
    Parameters
    ----------
    sp_data: dict keyed with zero-base sp numbers.
        stess period data
    reclist: list with with each item a cell record (k, j, i, ...)
        the permanent records to be added to each stress period
    idomain: ndarray of ints
        integer denoting cell type (active, inactive or flow-through)
        idomain is changed in place.
    """
    for iper in chd_spd:
        rec_list = chd_spd[iper]
        cells = {rec[0] for rec in rec_list}  # set
        for rec in chd_list:
            k, j, i = rec[0]
            if not (k, j, i) in cells:
                rec_list.append(rec)
            else: # replace head with that of rec, i.e. rec[1]
                idx = [ii for ii, x in enumerate(rec_list) if x[0]==(k, j, i)]
                for ii in idx: # There should be only 1 idx, but nevermind
                    rec_list[ii] = rec
    return chd_spd


def set_up_modflow_sim(yasin_mf6, simname=None, folders=None):
    """Set_up the data for the modflow mf6 simulation.
    
    Parameters
    ----------
    yasin_mf6: dict of
        model data
    simname: str
        basename of simulation
    src_dir: str
        path to directory to put the simulation files. 
    """
    sim_src_dir  = os.path.join(folders['src_python'], simname) # working directory simulation
    exe_path = os.path.join(folders['exe'], 'mf6.mac')
    version = 'mf6'
    name = 'yasin_mf6'

    # if the sim directory does not yet exist, make it.
    if not os.path.isdir(sim_src_dir):
        os.mkdir(sim_src_dir)

    os.chdir(sim_src_dir)

    # Tdis, time dicretization
    time_units = 'Days'
    start_datetime = '2021-05-22 18:00'
    pdata = yasin_mf6['tdis']
    nper = pdata['NPER']
    period_data = [pdata['NPER'], pdata['NSTP'], pdata['TSMULT']]

    gr = yasin_mf6['dis']['gr']

    idomain = yasin_mf6['dis']['idomain']

    # Layer properties:
    # There is too much vertical resistance in the aquifers if k33 == k;
    # all vertical resistance in the original Yasin model was in VCONT,
    # which are separate layers in the new model with their own vertical
    # conductivity has to match the original VCONT. So we have to
    # nullify (almost) the vertical resistance within the aquifers to
    # match with the original 1999 model.
 
    # Oc output control
    headfile    = "{}.hds".format(name)
    budgetfile  = "{}.cbb".format(name)
    saverecord  = [("HEAD", "ALL"), ("BUDGET", "ALL")]
    printrecord = ("HEAD", "LAST"),

    # IMS (interative ... solver)
    ims_kwargs = dict(
        print_option='ALL', # 'SUMMARY',
        complexity='COMPLEX',
        csv_outer_output_filerecord='ims_outer.csv',
        csv_inner_output_filerecord='ims_inner.csv',
        no_ptcrecord='FIRST', # 'ALL',  # or FIRST
        outer_maximum=200,
        outer_dvclose=0.1,
        under_relaxation='DBD',
        under_relaxation_theta=0.5,
        under_relaxation_kappa=0.3,
        under_relaxation_gamma=0.3,
        under_relaxation_momentum=0.001,
        backtracking_number=20,
        backtracking_tolerance=1.05,
        backtracking_reduction_factor=0.2,
        backtracking_residual_limit=10,

        inner_maximum=100,
        inner_dvclose=0.0001,
        rcloserecord=[10., 'STRICT'],
        linear_acceleration='BICGSTAB',
        relaxation_factor=0.97,  # may be zero (default)
        preconditioner_levels=5,
        preconditioner_drop_tolerance=1e-4,
        number_orthogonalizations=0,  # default = 0
        scaling_method='NONE', # 'L2NORM', # 'POLCG', # 'DIAGONAL', # 'NONE',
        reordering_method='NONE', # 'MD', # 'RCM', # 'NONE',
    )

    # Adding the packages this should normally stay untouched for any model
    # Except for which packagres to add. All the data are specified above.
    sim = flopy.mf6.MFSimulation(sim_name=name, exe_name=exe_path, version=version)

    tdis = flopy.mf6.ModflowTdis(sim, time_units=time_units,
                                start_date_time=start_datetime, nper=nper, perioddata=period_data)

    # Solver
    ims = flopy.mf6.ModflowIms(sim, pname='ims', **ims_kwargs)

    # Add the groundwater flow model
    gwf = flopy.mf6.ModflowGwf(sim, modelname=name, model_nam_file="{}.nam".format(name),
                            version=version, exe_name=exe_path, newtonoptions='under_relaxation',
                            )

    # Dis package
    dis = flopy.mf6.ModflowGwfdis(gwf,
                                nlay=gr.nlay, nrow=gr.nrow, ncol=gr.ncol, delr=gr.dx, delc=gr.dy, idomain=idomain,
                                top=gr.Ztop[0], botm=gr.Zbot)

    # Storage
    # sto = flopy.mf6.ModflowGwfsto(gwf, storagecoefficient=True, iconvert=1,
    #                              ss=ss, sy=sy, transient=transient,
    #                              save_flows=True
    #                              )
    # Initial conditions
    # Initial conditions
    strt = yasin_mf6['ic']['strt']
    ic = flopy.mf6.ModflowGwfic(gwf, strt=strt)

    # Layer rproperties
    npf = flopy.mf6.ModflowGwfnpf(gwf,
                            k   =yasin_mf6['npf']['k1'],
                            k33 = yasin_mf6['npf']['k3'],
                            icelltype = yasin_mf6['npf']['icelltype'],
                            save_flows=True,
                            )

    sto = flopy.mf6.ModflowGwfsto(gwf, save_flows=True,
                        storagecoefficient=False,
                        iconvert=yasin_mf6['sto']['iconvert'],  # per cell
                        ss=yasin_mf6['sto']['ss'],              # per cell
                        sy=yasin_mf6['sto']['sy'],              # per cell
                        steady_state=yasin_mf6['sto']['steady_state'], # per stress period
                        transient=yasin_mf6['sto']['transient'], # per stress period
                        )

    
    maxbound = lambda data: np.max(
                np.array([len(data[k]) for k in data], dtype=int))
    
    chd_spd = yasin_mf6['chd']['stress_period_data']
    chd = flopy.mf6.ModflowGwfchd(gwf,
            maxbound=maxbound(chd_spd),
            stress_period_data=chd_spd,
            save_flows=True
            )

    ghb_spd = yasin_mf6['ghb']['stress_period_data']
    ghb = flopy.mf6.ModflowGwfghb(gwf,
            maxbound=maxbound(ghb_spd), stress_period_data=ghb_spd, save_flows=True)

    if yasin_mf6['drn']['present']:
        drn_spd = yasin_mf6['drn']['stress_period_data']
        drn = flopy.mf6.ModflowGwfdrn(gwf,
            maxbound=maxbound(drn_spd), stress_period_data=drn_spd, save_flows=True)

    if yasin_mf6['drn']['present']:
        drn_spd = yasin_mf6['drn']['stress_period_data']

    wel_spd = yasin_mf6['wel']['stress_period_data']
    wel = flopy.mf6.ModflowGwfwel(gwf,
            maxbound=maxbound(wel_spd),
            auto_flow_reduce=0.1, timeseries=None,
            stress_period_data=wel_spd, save_flows=True)

    # For transient flow
    # rch = flopy.mf6.ModflowGwfrcha(gwf, save_flows=True, readasarrays=None,
    #                                 timearrayseries=None,
    #                                 recharge=rch_rec)

    # recharge
    # tas_name = 'ema_rech'
    # tas_fname = tas_name + '.tas'
    # interpolation_method = 'stepwise'
    # #tas = {0.0: 0.002, 1000.: 0.}
    # tas = {   0.0: yasin_mf6['rch']['stress_period_data'][0],
    #        1000.0: 0.}
    # tas.update(filename=tas_fname,
    #            time_series_namerecord=tas_name,
    #            interpolation_methodrecord=interpolation_method)

    # rcha = flopy.mf6.ModflowGwfrcha(gwf, save_flows=True, readasarrays=True,
    #                                 timearrayseries=tas, irch=get_irch(idomain, zero_based=True),
    #                                 recharge='timearrayseries ' + tas_name)
    rcha = flopy.mf6.ModflowGwfrcha(gwf, save_flows=True, readasarrays=True,
                                    timearrayseries=None,
                                    irch=yasin_mf6['rch']['irch'],
                                    recharge=yasin_mf6['rch']['stress_period_data'][0],
                                    )

    # Output control ('OC') package
    oc = flopy.mf6.ModflowGwfoc(gwf,
                                saverecord=saverecord,
                                head_filerecord=[headfile],
                                budget_filerecord=[budgetfile],
                                printrecord=printrecord,
                                )
    return sim


def run_simulation(sim):
    """Write simulation files and run the model.

    This should not have to be touched.
    """
    logging.warning("Runing simulation.")
    sim.write_simulation()

    # Run the model
    success, buff = sim.run_simulation()
    try:
        if not success:
            print("========================================")
            print("# MODFLOW 6 did not terminate normally. #")
            print("========================================")
            raise Exception("MODFLOW 6 did not terminate normally.")
    except:
        #mfsim_lst_fname = os.path.join(src_dir, 'yasin_mf6', 'mfsim.lst')
        #conv_test.iter_progress()
        pass


def post_process(yasin_mf6, HDS=None, iper=0, cmap='viridis', shps_dict=None, gis_dir=None):
    """Read and show the head results of the simulation in plane projection.

    Parameters
    ----------
    yasin_mf6: dict
        the model data
    HDS: flopy headsfile oject
        the heads from modflow read in by flopy
    iper: int
        stress period index (zero-based)
    cmap: str
        name of colormap to be using in colorbar
    shps_dict: dictionary
        names of the shape files to include in the maps
    """ 
    logging.warning("Postprocessing.")
    gr = yasin_mf6['dis']['gr']
    idomain = yasin_mf6['dis']['idomain']
    inactive = idomain <= 0
    
    # Last step of given iper
    kstpkper = [(ks, ip) for (ks, ip) in HDS.get_kstpkper() if ip==iper][-1]

    Zbot = np.ma.MaskedArray(data=gr.Zbot, mask=inactive)

    elev     = Zbot
    hstep = 100.
    elev_max = np.ceil( np.max(elev) / hstep) * hstep
    elev_min = np.floor(np.min(elev) / hstep) * hstep

    hraw = HDS.get_data(kstpkper=kstpkper)
    h = np.ma.MaskedArray(data=hraw,
            mask=np.logical_or(inactive, hraw <= elev))

    # Neat top and bottom for the ylim of the graphs
    hmax = np.ceil( np.max(h) / hstep) * hstep
    hmin = np.floor(np.min(h) / hstep) * hstep
    
    hd_levels = np.arange(hmin, hmax + hstep, hstep) #includes both hmin and hmax

    btypes    = ['chd', 'wel', 'ghb', 'drn', 'riv']
    clrs      = ['r', 'b', 'm', 'c', 'y']
    markers   = ['s', 'o', 'p', '^', 'v']
    rot = 30
    fsz = 5 # fontsize

    formation_name = [yasin_mf6['formations'][i]['name'] for i in range(gr.nz)]

    for layer in range(gr.nlay):
        ax = newfig('Heads for layer {}, [{}]'.format(
            layer, formation_name[layer]),
            'x_EPGS6984 [m]', 'y_EPGS6984 [m]', figsize=(6, 10), aspect=1)

        gr.plot_grid(ax=ax, lw=0.1, color='gray')

        c1 = ax.imshow(elev[layer], cmap=cmap, vmin=elev_min, vmax=elev_max,
                    extent=gr.extent, alpha=0.5)

        # Overlay with contour lines
        c2 = ax.contour(
            gr.xm, gr.ym, h[layer], levels=hd_levels, linestyles='-', colors="black", linewidths=0.5)
        ax.clabel(c2, fmt="%.0f m", fontsize=6)

        cb = plt.colorbar(c1, ax=ax)
        cb.ax.set_title('elevation [m]')

        for btype, marker, clr in zip(btypes, markers, clrs):
            plot_bounds(yasin_mf6, iper=iper, layer=layer, type=btype, ax=ax,
                            color=clr, mec=clr, mfc='none', fontsize=fsz,
                            marker=marker, rotation=rot)

        plot_objects(gis_dir=gis_dir, shps_dict=shps_dict, ax=ax)
    return None

def get_new_roof_of_model(gr_old, tif_path=None, ibound=None, plot=False):
    """Replace top of model with dem.

    Parameters
    ----------
    gr_old: fdm.Grid object
        the model grid
    tif_path: str
        Full path of the actual tif file with accurate grid.
        Notice that the file 'wbank_6984_km.tif has been prepared to
        cover the potential model area and has pixel size (1000, -1000)
        while the elevations are exactly at half-km interval in the
        new Israel-Palestine grid with EPGS6984.
        Therefore, a shift of the model should work seamlessly along as its
        cell size remains 1000 x 1000 m. Otherwize update the tif file
        using `read_tiffs_final.py`.
    plot: boolean
        produces plots if True
    out: Boolean
        if True return grid else return None
    """
    from rasterio.plot import show 

    with rasterio.Env():    
        with rasterio.open(tif_path, 'r') as src:
            # The source tif file has been prepared such that it has the proper coordinates
            # compatible with the of the model with ground surface as data.
            
            # Get the dem from fp_tif
            Ztop0 = src.read(1)
            if plot:
                ax = newfig("Dem over the entire potential model area",
                            "x [m] EPGS6984", "y [m] EPGS6984", aspect=1.0)
                show(src, ax=ax)

            # Compute the dem elevation coordinates
            xm = np.array([(src.transform * (i, 0))[0] for i in range(src.width)])
            ym = np.array([(src.transform * (0, i))[1] for i in range(src.height)])

            # Match with those of the current grid, and hence, the current model
            tol = 0.01
            Lx = np.logical_and(xm >= gr_old.xm[0] -tol, xm <= gr_old.xm[-1] + tol)
            Ly = np.logical_and(ym <= gr_old.ym[0] +tol, ym >= gr_old.ym[-1] - tol)

            # Select the part of the dem that matches the model grid exactly
            ztop = Ztop0[np.ix_(Ly, Lx)][np.newaxis, :, :]

            # ground surface correction, make layers at least 25 m thick
            # Set top to at least 25 m above each lower layer
            Zbot = gr_old.Zbot.copy()
            Zbot[ibound == 0] = -3000          # inactive must not have any impact on the outcome
            Zbot = np.max(Zbot, axis=0)        # maximum (=top) of bottoms
            ztop = np.fmax(ztop, Zbot + 25)    # makes layers at least 25 m thick unless inactive

            # Put this on top of the existing elevations
            Z = np.concatenate((ztop, gr_old.Zbot), axis=0)

            # Reconstruct the grid
            gr_new = Grid(gr_old.x, gr_old.y, Z, LAYCBD=gr_old.LAYCBD, min_dz=gr_old.min_dz)
    
            if plot:
                iz = 0
                ax = newfig("Ground-surface of the model",
                        'x [m] EPGS6984', 'y [m] EPGS6984', aspect=1.0)
                ax.imshow(gr_new.Ztop[iz], extent = gr_new.extent)

    return gr_new
     
# %% Cross sections

def plot_cross_sections(yasin_mf6, rows=None, step=True,
                    HDS=None, iper=None):
    """Plot model cross sections along given rows.
    
    Parameters
    ----------
    yasin_mf6: dict
        data for the yasin_mf6 model as a dictionary
    rows: sequence of ints
        numbers of the rows for which a cross section is desired.
    step: boolean
        if True plot patches as steps according to the MODFLOW grid.
        if False plot pathces by combining cell centers.
    HDS: HDS file object (flopy)
        The head file object holding the heads and related info.
    iper: int
        Stess period number of which the heads of the last
        time step will be plotted.
    """
    gr = yasin_mf6['dis']['gr']
    inactive = yasin_mf6['dis']['idomain'] <= 0

    if HDS:
        kskp = [kskp for kskp in HDS.get_kstpkper() if kskp[-1] == iper][-1]
        hds = np.ma.masked_array(HDS.get_data(kstpkper=kskp),
            mask=inactive)

    Ztop = np.ma.masked_array(gr.Ztop, mask=inactive)
    Zbot = np.ma.masked_array(gr.Zbot, mask=inactive)

    zstp = 100.
    vmax = np.ceil( np.max(Ztop) / zstp) * zstp
    vmin = np.floor(np.min(Zbot) / zstp) * zstp

    # plot layer cross sections using patches
    for row in rows:
        ax = newfig("Cross section Mountain Aquifer, row {}, y_EPGS6984 ={:.0f} m".format(row, gr.ym[row]),
                            "x[m] (EPGS6984)", "elev [m]", 
                            xlim = (gr.x[0], gr.x[-1]),
                            ylim=(vmin, vmax))
        # Plot the top (ground surface)
        if not step:
            ax.plot(gr.xm, Ztop.data[0, row], 'g-', lw=2)
        else:
            zt = np.vstack((gr.Ztop[0, row], gr.Ztop[0, row])).T.flatten()
            xt = np.vstack((gr.x[:-1], gr.x[1:])).T.flatten()
            ax.plot(xt, zt, '-g', lw=2)

        # reset the colors
        clrs = [yasin_mf6['formations'][i]['color'] for i in range(gr.nz)]

        for iz in range(gr.nz):
            fc = clrs[iz]

            I = np.arange(gr.nx, dtype=int)[inactive[iz, row] == False]
            if len(I) == 0:
                continue

            if not step: # Old
                # Make I continuous in case this is not the case
                # Vertical patch contour:
                xm = np.hstack((gr.xm[I], gr.xm[I[::-1]], gr.xm[I[0]]))
                z  = np.hstack((Zbot[iz, row, I],
                                Ztop[iz, row, I[::-1]],
                                Zbot[iz, row, I[0]]))
                # Turn coordinates into a Path
                I = np.hstack((I, I[::-1], I[0]))
                vertices = np.vstack((xm, z)).T
                codes = np.ones(len(I), dtype=int)
                codes[0]  = patches.Path.MOVETO
                codes[1:] = patches.Path.LINETO
                codes[-1] = patches.Path.CLOSEPOLY

            else: # New
                # Turn this in a step curve that is also closed
                zb = np.vstack((gr.Zbot[iz, row, I],
                                        gr.Zbot[iz, row, I])).T.flatten()
                zt = np.vstack((gr.Ztop[iz, row, I],
                                        gr.Ztop[iz, row, I])).T.flatten()[::-1]
                xb = np.vstack((gr.x[:-1][I], gr.x[1:][I])).T.flatten()
                xt = np.vstack((gr.x[:-1][I], gr.x[1:][I])).T.flatten()[::-1]
                z = np.hstack((zb, zt, zb[0]))
                x = np.hstack((xb, xt, xb[0]))
                vertices = np.vstack((x, z)).T

                # Turn coordinates into a Path
                codes = np.ones(len(vertices), dtype=int)
                codes[0]  = patches.Path.MOVETO
                codes[1:] = patches.Path.LINETO
                codes[-1] = patches.Path.CLOSEPOLY

            # Generate patch and add to ax
            p = patches.PathPatch(patches.Path(vertices=vertices,
                            codes=codes, closed=True),
                            facecolor=fc, ec='gray', alpha=1)
            ax.add_patch(p)

        if HDS:
            lss = line_cycler()
            for iz in range(gr.nz):
                ls = next(lss)
                ax.plot(gr.xm, hds[iz, row, :], color='k', ls=ls, label='head layer {}'.format(iz))

        F=yasin_mf6['formations']
        p = [patches.Patch(color=F[i]['color'], label=F[i]['name'])
                            for i in range(gr.nz)]
        leg = ax.legend()
        ax.legend(handles=leg.legendHandles + p)

    return None

# %% Show the model
def show_model(yasin_mf6, gis_dir, iper=0, useimshow=True, showDZ=True):
    """Show the model elevation of layer thickness with ghb locations and heads.
    
    Parameters
    ----------
    yasin_mf6: Dict
        Model Data
    gis_dir: str (path)
        Directory with the shape files.
    iper: Int
        Stress period number (zero-based)
    useimshow: Boolean
        Use imshow if true, else use contourf.
    showDZ: Boolean
        Show DZ else show Zbot
    """
    gr = yasin_mf6['dis']['gr']
    inactive = yasin_mf6['dis']['idomain'] <= 0

    formation_name = [yasin_mf6['formations'][i]['name'] for i in range(gr.nz)]

    if showDZ:
        A = np.ma.masked_array(gr.DZ, mask=inactive, fill_value=-9999.99)
        title = "Thickness of model layer {}, [{}]"
    else:
        A = np.ma.masked_array(gr.Zbot, mask=inactive, fill_value=-9999.999)
        title = "Elevation of bottom of model layer {} [{}]"
    for iz in range(gr.nz): # for all aquifers, note: aquifers start at 1.
        chd_spd = yasin_mf6['chd']['stress_period_data']
        ax = newfig(title.format(iz, formation_name[iz]),
                    "x_EPGS6984 [m]", "y_EPGS6984 [m]", figsize=(6, 10))

        if useimshow:
            c = ax.imshow(A[iz], extent=gr.extent, alpha=1)
        else:
            c = ax.contourf(gr.xm, gr.ym, A[iz])

        # Plot the ghb points for period iper.
        for rec in chd_spd[iper]:
            (iz_mf, iy, ix), h_ = rec
            # But only if point is in current aquifer.
            if iz_mf == iz + 1:
                ax.plot(gr.xm[ix], gr.ym[iy], 'ro')
                ax.text(gr.xm[ix], gr.ym[iy], ' {} m'.format(str(h_)))

        plot_objects(gis_dir=gis_dir, shps_dict=shps_dict, ax=ax)

        # Contour elev of aquifer
        plt.colorbar(c, ax=ax) # For elevation.


#%% Verifications

def verify(yasin_mf6, HDS, CBC):
    """Verify aspects of the model hydrologically.
    """
    NOT = np.logical_not
    print("Verify the recharge:")

    gr = yasin_mf6['dis']['gr']

    # Surface area occipied by active cells is
    idomain = yasin_mf6['dis']['idomain']

    # Total reacharge specified
    irch1 = get_irch(idomain, zero_based=False)
    active = irch1 !=0

    # Total recharge on active area
    RECH = yasin_mf6['rch']['stress_period_data'][0]
    rch_tot       = np.sum(RECH * gr.Area)
    rch_tot_act   = np.sum(RECH * gr.Area * active)
    rch_tot_inact = np.sum(RECH * gr.Area * NOT(active))
    act_frac = np.sum(active * gr.Area) / np.sum(gr.Area)

    print("Active frac of model area     = {:8.4f} [ - ]".format(act_frac))
    print("Total recharge model area     = {:8.4f} m3/d".format(rch_tot))
    print("Total recharge active area    = {:8.4f} m3/d".format(rch_tot_act))
    print("Total recharge inactive area  = {:8.4f} m3/d".format(rch_tot_inact))

    # Wells:
    WEL = yasin_mf6['wel']['stress_period_data'][0]
    Q1 = np.array([w[1] for w in WEL if w[0][0] == 1]).sum()
    Q3 = np.array([w[1] for w in WEL if w[0][0] == 3]).sum()
    Qt = np.array([w[1] for w in WEL]).sum()
    print("Qwells layer 1 = {:8.4f}".format(Q1))
    print("Qwells layer 3 = {:8.4f}".format(Q3))
    print("Qwells total   = {:8.4f}".format(Qt))
    
    # General head boundary
    GHB = yasin_mf6['ghb']['stress_period_data'][0]
    hds = HDS.get_data(kstpkper=HDS.get_kstpkper()[-1])
    ghb = np.array([(hds[w[0]], w[1], w[2]) for w in GHB])
    Qghb = np.sum((ghb[:, 1] - ghb[:, 0]) * ghb[:, 2])
    print("Qghb total = {:8.4f}".format(Qghb))

    # Using the CellBudgetFile
    record_names = ['WEL', 'GHB', 'CHD', 'RCH']
    kstpkper = CBC.get_kstpkper()[-1]
    for recname in record_names:
        recs = CBC.get_data(kstpkper=kstpkper, text=recname)[0]
        qin = recs['q'][recs['q'] > 0]
        qout= recs['q'][recs['q'] < 0]
        print("Q {} = in: {:8.4f} out: {:8.4f}".format(
            recname, np.sum(qin), np.sum(qout)))

#%%
def nodes2lrc(nodes, shape, zero_based=True):
    """Return LRC list of tuples given nodes and shape of regular grid.
    
    nodes: array of ints
        the node numbers
    shape: 3-tuple
        the shape of the model (layer, row, col)
    """
    _, nrow, ncol = shape

    # The node numbers are 1-based, so treat them as such.
    # Convert to zero-based lrc afterwards if so desired
    ilay = np.asarray(np.floor(nodes / (nrow * ncol)), dtype=int) + 1
    irest = nodes - (ilay - 1) * nrow * ncol
    irow = np.asarray(np.floor(irest / ncol), dtype=int) + 1
    icol = irest - (irow - 1) * ncol
    if zero_based:
        return [(il - 1, ir - 1, ic - 1) for il, ir, ic in zip(ilay, irow, icol)]
    else:
        return [(il, ir, ic) for il, ir, ic in zip(ilay, irow, icol)]

#%%
def analyze_convergence(yasin_mf6, HDS, iper=0, showDZ=False):
    """Anlayze the convergence by location of max_dv an dmax_dr."""

    # The location of the output files of modflow:
    src_dir = '/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/models/Yasin_1999_Eastern_Basin/python/'
    data = os.path.join(src_dir, 'yasin_mf6')

    gr = yasin_mf6['dis']['gr']

    inactive = yasin_mf6['dis']['idomain'] <= 0
    

    Zbot = np.ma.masked_array(data=gr.Zbot, mask=inactive)

    kstpkper = [kskp for kskp in HDS.get_kstpkper() if kskp[-1]==iper][-1]
    hds = np.ma.masked_array(HDS.get_data(kstpkper=kstpkper), mask=inactive)
    vstp = 100.
    vmin = np.floor(Zbot.max() / vstp) * vstp
    vmax = np.ceil( Zbot.min() / vstp) * vstp

    dstp = 10.
    DZ   = np.ma.masked_array(data=gr.DZ,   mask=inactive)
    dmin, dmax = 0, np.ceil(DZ.max() / dstp) * dstp

    # The output of the IMS, showing progress of convergence
    ims_inner = os.path.join(data, 'ims_inner.csv')
    ims_outer = os.path.join(data, 'ims_outer.csv')

    ii = pd.read_csv(ims_inner) # Follow inner iterations
    io = pd.read_csv(ims_outer) # TODO Follow the outer iterations

    # Show progress of convergence
    ax = newfig("Solution_inner_dvmax", "total_inner_iterations", "solution_inner_dvmax", yscale='log')
    ax.plot(ii['total_inner_iterations'], ii['solution_inner_dvmax'],
     label='dvmax', lw=0.25)
    ax.plot(ii['total_inner_iterations'], ii['solution_inner_drmax'],
     label='drmax', lw=0.25)

    ax = newfig("Solution_inner_dvmax_node", "total_inner_iterations", "solution_inner_dvmax", yscale='log')
    ax.plot(io['total_inner_iterations'], io['solution_outer_dvmax'],
     label='dvmax', lw=0.25)

    # Show where the maximum error occurs
    lrc_idv = np.asarray(nodes2lrc(ii['solution_inner_dvmax_node'].values,
     shape=gr.shape, zero_based=True), dtype=int).T
    lrc_idr = np.asarray(nodes2lrc(ii['solution_inner_drmax_node'].values,
     shape=gr.shape, zero_based=True), dtype=int).T
    lrc_odv = np.asarray(nodes2lrc(io['solution_outer_dvmax_node'].values,
     shape=gr.shape, zero_based=True), dtype=int).T

    btypes = ['chd', 'wel', 'ghb', 'drn', 'riv']
    bclrs  = ['r', 'b', 'm', 'c', 'y']
    bmarkers   = ['s', 'o', 'p', '^', 'v']
    rot = 30
    fsz = 7 # fontsize

    # Find the locations where the layer thickness is gr.min_dz, while
    # idomain >1. These are suspected causing trouble with convergence.
    # TODO: find the origin of this nuisanse.
    active = yasin_mf6['dis']['idomain'] > 0
    cells_too_thin = np.isclose(gr.DZ, gr.min_dz)
    bad = np.logical_and(active, cells_too_thin)
    assert not gr.LRC(gr.NOD[bad]),\
        "There must be no active cells with thickness <= gr.min_dz"

    # Plot the boundary points, so where they match the largest errors
    # Especially wells may be pumped more than the aquifer can deliver.
    # We can check this automatically or let the well be reduced until
    # its water level remains above the bottom of the aquifer.
    for ilay in range(gr.shape[0]):
        clrs = ['w', 'y','m']
    
        ax = newfig("location of max error, ilay={})".format(ilay),
                            "x [m]", "y [m]", figsize=(6, 10))

        if not showDZ:     
            c = ax.imshow(Zbot[ilay], extent=gr.extent, aspect='equal',
                            vmin=vmin, vmax=vmax, cmap='viridis')
        else:
            c = ax.imshow(DZ[ilay], extent=gr.extent, aspect='equal',
                            vmin=dmin, vmax=dmax, cmap='viridis')

        ax.plot(gr.xm[lrc_idv[2][lrc_idv[0]==ilay]],
                  gr.ym[lrc_idv[1][lrc_idv[0]==ilay]],
            color=clrs[0],  ls='none', marker='.', label='inner_dv')

        ax.plot(gr.xm[lrc_idr[2][lrc_idr[0]==ilay]],
                  gr.ym[lrc_idr[1][lrc_idr[0]==ilay]],
            color=clrs[1], ls='none', marker='.', label='inner_dr')

        ax.plot(gr.xm[lrc_odv[2][lrc_odv[0]==ilay]],
                  gr.ym[lrc_odv[1][lrc_odv[0]==ilay]],
            color=clrs[2], ls='none', marker='.', label='outer_dv')
        
        for btype, clr, marker in zip(btypes, bclrs, bmarkers):
            plot_bounds(yasin_mf6, iper=iper, layer=ilay, type=btype, ax=ax,
                            color=clr, mec=clr, mfc='none', fontsize=fsz,
                            marker=marker, rotation=rot)

        plt.colorbar(c, cax=None, ax=ax)

        ax.legend()

        plot_objects(gis_dir=gis_dir, shps_dict=shps_dict, ax=ax)

    return

# %%
if __name__ == '__main__':
    logging.warning("Running main in file {}".format(__file__))
    newmodel = 'yasin_mf6'

    strategy = 3 # Boundary-head correction stratagy to ensure head in within correct layer

    showDZ = False # Shows DZ in cross sectoins else shows Zbot

    rows = [2, 5, 10, 30, 50, 63, 70] # Rows for which to show cross sections
    
    if 'Mountain peaks.shp' in shps_dict.keys():
        shps_dict.pop('Mountain peaks.shp')

    # The tiff holding ground surface elevation in EPGS6964 with km grid
    gr_surface_tif = os.path.join(folders['src_python'], 'wbank_6984_km.tif' )

    # Generat the yasin model in mf6 (its grid and data)
    yasin_mf6 = get_new_mf6_model(yasin_old, tif_path=gr_surface_tif, chd_dead_sea_coast=None, drains_present=False, strategy=strategy)

    # Show what the new model looks like (maps, cross sections)
    show_model(yasin_mf6, folders['gis'], useimshow=True, showDZ=showDZ)

    #plot_cross_sections(yasin_mf6, rows=rows, HDS=None, iper=iper, step=True)

    # Instantiate a modflow 6 simulation object
    sim = set_up_modflow_sim(yasin_mf6, simname=newmodel, folders=folders)
    run_simulation(sim)

    # Read the binary head and cell-by-cell budget files
    headfile    = "{}.hds".format(newmodel)  # Name of the head file produced by modflow 6
    budgetfile  = "{}.cbb".format(newmodel)  # Name of the budget file produced by modflow 6

    HDS = flopy.utils.binaryfile.HeadFile(headfile)  # Read the binary head file
    CBC = flopy.utils.binaryfile.CellBudgetFile(budgetfile) # Read the binary cell-by-cell budget file

    # Choose a stress period to show the simulation results
    iper = 0

    plot_cross_sections(yasin_mf6, rows=rows, HDS=HDS, iper=iper, step=True)  # Step is True --> cross sections shown with stepped elevations (better)
    #plot_cross_sections(yasin_mf6, rows=rows, HDS=HDS, iper=iper, step=False) # Step is Ffalse --> cross section shown with continuous elevations (not so good)
    

    # Post-processing, i.e. contouring the heads in the different layers
    post_process(yasin_mf6, HDS=HDS, iper=iper, cmap='viridis', shps_dict=shps_dict, gis_dir=folders['gis'])

    # %% Carry out some verifications (see function)
    verify(yasin_mf6, HDS, CBC)

    # %% Show some figures to follow the progress of the convergence 
    #analyze_convergence(yasin_mf6, HDS, iper=0, showDZ=showDZ)

    #print("Done1")
    plt.show(block=True)

# %%
