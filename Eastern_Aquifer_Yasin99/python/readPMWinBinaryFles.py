# Reading PMWin binary files, see description on PMWIN.NET

# %% inputes
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import struct
import mkpath # own local model to set paths to own modules elsewhere on the HD
from pmwin_binary_files import * # PMWIN file type utilities
from etc import newfig, attr # own module with some useful functions

# %% Paths
proj  = '/Users/Theo/Instituten-Groepen-Overleggen/HYGEA/Hygea-Consult/2020_BUZA_ISR_PAL_T/'
yasin = os.path.join(proj, 'models/Yasin_1999_Eastern_Basin/')
pmwin_data_path = os.path.join(yasin, 'pm5_1_model_files/')
basename = 'trial2'

print()
if os.path.exists(pmwin_data_path):
    print('pmwin data files are in:')
    print(pmwin_data_path)
    print()
else:
    print("path does not exist:")
    print(pmwin_data_path)
    print()

try_ext = '.bot'
if os.path.isfile(os.path.join(pmwin_data_path, basename + try_ext)):
    print('By example, file `{}` exists in the previous data directory.'.format(
        os.path.basename(os.path.join(pmwin_data_path, basename + try_ext))))

# %% Model
# The extent of the model
extent = (200000.0, 247000.0, 587000.0, 685000.0)

# The shape of the model
shape = (4, 98, 47)
nlay, nrow, ncol = shape

print("Model extent is [m]:")
print(extent)
print()
print("Model size (shape) [nLay, nRow, nCol] =", print(shape))
print()

# %% functions

def show_layer_data(data3d, layer_type='datatype', extent=None, vmin=None, vmax=None, cmap=None):
    """Return imshow of the data layers.

    Parameters
    ----------
    data3d: ndarray
        The 3d array to be shown.
    typer_type: str (optional)
        The user description of the layer type.
    extent: tuple of 4 values (ll, ur)
        The horizontal extent of the model (layer) in user units.
    vmin: float (Optional)
        minimum values in colorbar
    vmax: float (Optional)
        maximum value in colorbar
    cmap: str (optional)
        name of colormap
    """
    if vmin is None: vmin = np.min(data3d)
    if vmax is None: vmax = np.max(data3d)
    if cmap is None: cmap = 'viridis'
    for iz in range(len(data3d)):
        ax = newfig('{} {}'.format(layer_type, iz), 'x [m]', ['r [m]'])
        ax.imshow(data3d[iz], extent=extent, vmin=vmin, vmax=vmax, cmap=cmap)
    return ax


def get_list_of_cbc_files(pdata):
    """Return list of PMWIN filesbinary files with CBC internal structure."""
    hdr_fmt = '1000i' # according to PMWIN, first 1000x4 bytes header
    names = []
    for fname in os.listdir(pdata):
        with open(os.path.join(pdata, fname), 'rb') as fp:
            try:
                hdr_tp = struct.unpack(hdr_fmt, fp.read(struct.calcsize(hdr_fmt)))
                if np.all(hdr_tp[:10] == hdr_tp[:10]):
                    print(fname)
                    names.append(fname)
            except:
                pass
    return sorted([os.path.splitext(name)[1][1:] for name in names])
    
# %%
os.path.splitext('trial2.sbo')

# %%
def getObsdata(fname, shape=None, verbose=False):
    """Return contents of binary observationf ile (extension 'pol').

    Ths is a trial to decipher its binary contents. Still no success.
    TO 20210716
    """
    hdr_fmt = '18sh'
    rec_fmt = '26c'
    borefmt = '8shiiihh'
    boresize = struct.calcsize(borefmt)
    bores = []
    observations = []
    with open(fname, 'rb') as fp:
        flen = fp.seek(0, 2) # rewind
        fp.seek(0, 0)
        labelStr = struct.unpack('18s', fp.read(18))[0].decode('u8')
        option   = struct.unpack('h', fp.read(2))[0]
        print('labelStr {:s}, option {:d}'.format(labelStr, option))
        
        if labelStr == 'PMWIN4000_OBS_FILE':
            for i in range(1000):
                # nm, a, x, y, lay, plotit, plotclr <--- one bore
                bore = struct.unpack(borefmt, fp.read(boresize))
                bores.append(bore)
        elif labelStr == 'PMWIN5000_OBS_FILE':
            for i in range(1000):
                bore = struct.unpack('=8shffihh', fp.read(26))
                bores.append(bore)
            for iobs in range(6000):
                obs1 = struct.unpack('=8sh8f', fp.read(42))
                observations.append(obs1)
            print('bore stored.')
        else:
            pass
    return bores, observations

# %%
obdata, obs = getObsdata(os.path.join(pmwin_data_path, basename + '.pol'), shape=shape, verbose=False)

# It is concluded that this file does not contain any useful data.

# %%
